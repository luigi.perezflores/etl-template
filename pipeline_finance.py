import sys
import pandas as pd
import time

from src.finance.provisions import run_pipeline as run_provisions
from config_pipeline import (
    dir_path_finance,
)

def run_pipeline(
    vintage:int
):
    start = time.time()
    print("ETL de provisiones ha iniciado")

    run_provisions(
        end_date=vintage,
        file_path=dir_path_finance
    )

    end = time.time()
    duration_time = end - start

    print(f"Tiempo total: {duration_time:.1f} segundos")

if __name__ == "__main__":
    vintage = sys.argv[1]
    run_pipeline(
        vintage
    )