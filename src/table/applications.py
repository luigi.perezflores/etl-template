import os
import re
import datetime as dt
import pandas as pd
import numpy as np
import pickle

from boasci.google import Spread

from utils.utils import format_column_name, rename_cols, filter_columns
from constants.applications import (
    data_sources_google,
    str_columns_1,
    str_columns_2,
    rename_columns_dict,
    final_vars_dict,
    date_columns_dict,
    str_cols,
    numeric_cols,
    data_types,
)


# Carga de datos
## Fuente Google Sheet Waldos Motos
def load_data_from_spread(source_version: int, sheet_name: str) -> pd.DataFrame:
    df = Spread(
        data_sources_google[source_version]["id"],
        mode="r",
        pull_kwargs={"evaluate_formulas": True},
        sheet_name=sheet_name,
    )[sheet_name].copy()

    return df


def format_string_columns(df: pd.DataFrame) -> pd.DataFrame:
    df_aux = df.copy()

    for var in str_columns_1:
        df_aux[var] = df_aux[var].apply(
            lambda x: re.sub(" +", " ", str(x)).strip().capitalize()
        )

    for var in str_columns_2:
        df_aux[var] = df_aux[var].apply(
            lambda x: re.sub(" +", " ", str(x)).strip().title()
        )

    return df_aux


def filter_data(df: pd.DataFrame) -> pd.DataFrame:
    df_aux = df.copy()
    df_aux = df_aux[
        (df_aux.id_cliente.notna())
        & (
            df_aux.estado_final.isin(
                ["Moto entregada", "Aprobado", "Rechazado", "Proceso incompleto"]
            )
        )
    ]

    return df_aux


def transform_date_cols(df: pd.DataFrame, date_columns: list) -> pd.DataFrame:
    df_aux = df.copy()
    for var in date_columns:
        df_aux[var] = pd.to_datetime(df_aux[var], dayfirst=True)

    return df_aux


def transform(
    df: pd.DataFrame,
) -> pd.DataFrame:
    df_aux = df.copy()
    # store_id
    df_aux["store_id"] = df_aux["tienda"].str.split("-").str[0].fillna("")

    # status_id
    condiciones = (
        lambda x: 1
        if x == "Moto entregada"
        else 2
        if x == "Aprobado"
        else 3
        if x == "Rechazado"
        else 4
        if x == "Proceso incompleto"
        else None
    )
    df_aux["status_id"] = df_aux["estado_final"].apply(condiciones)

    # booked
    condiciones = lambda x: 1 if x == "Moto entregada" else 0
    df_aux["booked"] = df_aux["estado_final"].apply(condiciones)

    # completed
    condiciones = lambda x: 1 if x != "Proceso incompleto" else 0
    df_aux["completed"] = df_aux["estado_final"].apply(condiciones)

    # risk_decision
    df_aux["estado_final"] = df_aux["estado_final"].fillna("")
    condiciones = (
        lambda x: 1
        if x == "Aprobado" or x == "Moto entregada"
        else 0
        if x == "Rechazado"
        else -1
    )
    df_aux["risk_decision"] = df_aux.estado_final.apply(condiciones)

    # rejection_cause_risk
    df_aux["causal_rechazo/aceptación"] = df_aux["causal_rechazo/aceptación"].fillna("")
    condiciones = (
        lambda x: x
        if x == "Los score no superan el umbral para aprobación"
        or x == "Su capacidad de pago no es suficiente"
        else None
    )
    df_aux["rejection_cause_risk"] = df_aux["causal_rechazo/aceptación"].apply(
        condiciones
    )

    # rejection_cause_kyc
    condiciones = (
        lambda x: x
        if x == "No se pudo validar el domicilio"
        or x == "No se pudo validar la ocupación"
        else None
    )
    df_aux["rejection_cause_kyc"] = df_aux["causal_rechazo/aceptación"].apply(
        condiciones
    )

    # rejection_cause
    condlist = [
        (~df_aux["rejection_cause_risk"].isna()),
        (~df_aux["rejection_cause_kyc"].isna()),
    ]
    choicelist = ["risk", "kyc"]
    df_aux["rejection_cause"] = np.select(condlist, choicelist, default=None)

    # frequency
    df_aux["frequency"] = 2

    # approved
    condlist = [
        (df_aux["rejection_cause"].isna()) & (df_aux["risk_decision"] == 1),
    ]
    choicelist = [1]
    df_aux["approved"] = np.select(condlist, choicelist, default=0)

    return df_aux


def run_pipeline(source_version: int, file_path: str) -> pd.DataFrame:
    df_resumen = load_data_from_spread(source_version, "Resumen data")
    df_pay_cap = load_data_from_spread(source_version, "Capacidad pago")
    df_pay_cap = df_pay_cap[["id_cliente", "Remanente"]]

    df = df_resumen.merge(df_pay_cap, on="id_cliente")

    df = format_column_name(df)
    df = format_string_columns(df)
    df = filter_data(df)
    df = transform_date_cols(df, date_columns_dict[source_version])
    df = transform(df)
    df = rename_cols(df, rename_columns_dict[source_version])
    df = filter_columns(df, final_vars_dict[source_version])
    df = df.dropna(subset='id', how='all')
    df[numeric_cols] = df[numeric_cols].apply(pd.to_numeric, errors="coerce")
    df = df.replace({np.nan: None})
    df[str_cols] = df[str_cols].fillna("")
    df[str_cols] = df[str_cols].replace('<Na>', '')
    df = df.astype(data_types)
    df.to_csv(file_path, index=False, sep=";")

    return df