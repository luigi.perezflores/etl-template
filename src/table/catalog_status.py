import pandas as pd
import datetime as dt

# Crear lista con los valores de la columna "name"
NAME_VALUES = ["Moto entregada", "Aprobado", "Rechazado", "Proceso incompleto"]

# Crear lista con las descripciones de cada valor de la columna "name"
DESC_VALUES = [
    "La moto ha sido entregada al cliente",
    "El proceso ha sido aprobado",
    "El proceso ha sido rechazado",
    "El proceso no ha sido completado",
]


def run_pipeline(source_version: int, file_path: str) -> pd.DataFrame:
    id_values = list(range(1, len(NAME_VALUES) + 1))
    data = {"id": id_values, "name": NAME_VALUES, "description": DESC_VALUES}
    df = pd.DataFrame(data)
    df.set_index("id", inplace=True)
    df.to_csv(file_path, index=False)

    return df