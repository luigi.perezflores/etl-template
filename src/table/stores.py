import pandas as pd

import requests
from bs4 import BeautifulSoup

from src.constants.stores import ( 
    url_stores,
    str_columns_2,
    str_columns_3,
    integer_columns,
    rename_columns,
    final_vars
)


lst_exclusive_stores = [1161, 1180, 1168, 1158]

from src.utils.utils import format_column_name, format_to_int, rename_cols, filter_columns

def load_data_from_web(
        url_stores:str
    ) -> pd.DataFrame:
    page = requests.get(url_stores)
    soup = BeautifulSoup(page.text, 'lxml')

    table1 = soup.find('table')

    headers = []
    for i in table1.find_all('tr'):
        title = [x for x  in i.text.split('\n') if x != '']
        headers.append(title)
        
    stores_data = pd.DataFrame(headers[1:], columns = headers[0])

    return stores_data


def format_string_columns(
        df:pd.DataFrame
    ) -> pd.DataFrame:
    df_aux = df.copy()
        
    for var in str_columns_2:
        df_aux[var] = df_aux[var].str.capitalize().str.strip()
        
    for var in str_columns_3:
        df_aux[var] = df_aux[var].str.title().str.strip()

    return df_aux

def assign_exclusives(
        df:pd.DataFrame,
        exclusive_stores:list
    ):

    df["exclusive"] = df.id.isin(exclusive_stores)

    return df



def run_pipeline(
        file_path:str
    ) -> pd.DataFrame:
    df = load_data_from_web(url_stores)
    df = format_column_name(df)
    df = format_string_columns(df)
    df = rename_cols(df, rename_columns)
    df = format_to_int(df, integer_columns)
    df = filter_columns(df, final_vars)
    df = assign_exclusives(df,lst_exclusive_stores)

    df.to_csv(
        file_path,
        index=False
    )

    return df