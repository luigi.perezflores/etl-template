import os
import re
import datetime as dt
import pandas as pd
import numpy as np
import pickle

from boasci.google import Spread

from utils.utils import format_column_name, rename_cols, filter_columns
from constants.customers import (
    data_sources_google,
    rename_columns_dict,
    str_cols,
    numeric_cols,
    data_types,
    final_vars_dict
)

# Carga de datos
## Fuente Google Sheet Waldos Motos
def load_data_from_spread(source_version: int, sheet_name: str) -> pd.DataFrame:
    df = Spread(
        data_sources_google[source_version]["id"],
        mode="r",
        pull_kwargs={"evaluate_formulas": True},
        sheet_name=sheet_name,
    )[sheet_name].copy()

    return df

def run_pipeline(source_version: int, file_path: str) -> pd.DataFrame:
    df = load_data_from_spread(1, "Formulario")

    df = format_column_name(df)
    df = rename_cols(df, rename_columns_dict[1])
    df = filter_columns(df, final_vars_dict[1])
    df = df.dropna(subset='application_id', how='all')
    df[numeric_cols] = df[numeric_cols].apply(pd.to_numeric, errors="coerce")
    df = df.replace({np.nan: None})
    str_cols = df.select_dtypes(include=['object']).columns
    df[str_cols] = df[str_cols].fillna("")
    df[str_cols] = df[str_cols].replace('<Na>', '')
    df = df.astype(data_types)
    df["email"] = df["email"].str.lower()
    df.to_csv(file_path, index=False, sep=";")
    
    return df