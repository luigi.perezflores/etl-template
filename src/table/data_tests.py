import pandas as pd

def check_missing_values(
        df:pd.Dataframe,
        cols:list|None=None,
    ) -> bool:
    """
    La función revisa si hay missings en un conjunto o todas las variables.
    """
    if cols is None:
        missing = df.isnull().sum().any()
    else:
        missing = df[cols].isnull().sum().any()

    return missing

def check_duplicates(
        df:pd.DataFrame, 
        cols:list|None=None,
    ) -> bool:
    """
    La función revisa si hay filas duplicadas, basado en un conjunto de variables o todas las variables.
    """
    duplicates = df.duplicated(cols).any()
    return duplicates

def check_data_types(
        df: pd.DataFrame,
        types:dict
    ) -> bool:
    """
    La función revisa si el tipo de dato en cada columna del diccionario coincide con el conjunto de datos.
    """
    actual_types = df.dtypes.to_dict()
    for col, dtype in types.items():
        if actual_types[col] != dtype:
            return False
    return True

def check_invalid_values(
        df: pd.DataFrame,
        col:str,
        valid_values:list
    ) -> bool:
    """
    La función revisa si los diferentes valores contenidos en una columna especifica se encuentran dentro de un conjunto de valores definidos.
    """
    invalid_values = ~df[col].isin(valid_values)
    return invalid_values.any()