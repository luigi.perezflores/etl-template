import pandas as pd
import numpy as np
from pandas.api.types import is_datetime64_any_dtype


from boasci.google import Spread
import re

from src.constants.loans import (
    data_sources_google,
    str_columns_1, 
    str_columns_2,
    str_columns_3,
    str_columns_4, 
    timestamp_columns_dict, 
    date_columns_dict,
    rename_columns_dict,
    periodos_dict,
    final_vars_dict,
    integer_columns,
    values_as_nan,
    numeric_columns_dict,
    manual_apps_pricing
)

from src.constants.constants import dict_tasas

from src.utils.utils import format_column_name, format_to_int, rename_cols, filter_columns


def load_data_from_spread(
        source_version:int
    ) -> pd.DataFrame:

    df = Spread(
        data_sources_google[source_version]['id'], 
        mode='r', 
        pull_kwargs={'evaluate_formulas': True},
        sheet_name=data_sources_google[source_version]['sheet_name']
    )[data_sources_google[source_version]['sheet_name']].copy()
    
    return df

def load_data_from_local(
        source_version:int,
        vintage:int
    ) -> pd.DataFrame:

    df = pd.read_excel(
        data_sources_google[source_version]['file'].format(vintage=vintage),
        sheet_name=data_sources_google[source_version]['sheet_name']
    ).copy()
    
    return df

def format_string_columns(
        df:pd.DataFrame
    ) -> pd.DataFrame:

    df_aux = df.copy()
    for var in str_columns_1:
        df_aux[var] = df_aux[var].apply(lambda x: re.sub(' +', ' ', str(x)).strip().lower())
        
    for var in str_columns_2:
        df_aux[var] = df_aux[var].apply(lambda x: re.sub(' +', ' ', str(x)).strip().capitalize())
        
    for var in str_columns_3:
        df_aux[var] = df_aux[var].apply(lambda x: re.sub(' +', ' ', str(x)).strip().title())
        
    for var in str_columns_4:
        df_aux[var] = df_aux[var].apply(lambda x: re.sub(' +', ' ', str(x)).strip().upper())


    df_aux = df_aux.replace(values_as_nan, np.NaN)

    return df_aux

def filter_data(
        df:pd.DataFrame
    ) -> pd.DataFrame:

    df_aux = df.copy()
    df_aux = df_aux[
        (df_aux.id_cliente.notna())
        & (df_aux.estado_final == 'Moto entregada')
    ]

    return df_aux

def transform_timestamp_cols(
        df:pd.DataFrame,
        timestamp_columns:list
    ) -> pd.DataFrame:

    df_aux = df.copy()
    for var in timestamp_columns:
        if is_datetime64_any_dtype(df[var]):
            pass
        else:
            df_aux[var] = pd.to_datetime(
                df_aux[var].astype(str).str.split(" ").str[0],
                dayfirst=True
            )

    return df_aux

def transform_date_cols(
        df:pd.DataFrame, 
        date_columns:list
    ) -> pd.DataFrame:

    df_aux = df.copy()
    for var in date_columns:
        df_aux[var] = pd.to_datetime(
            df_aux[var],
            dayfirst=True
        )

    return df_aux

def transform_numeric_cols(
        df:pd.DataFrame, 
        numeric_columns_dict:dict
    ) -> pd.DataFrame:

    df_aux = df.copy()
    for col, dtype in numeric_columns_dict.items():
        try:
            df_aux[col] = df_aux[col].astype(dtype)
        except:
            df_aux[col] = df_aux[col].astype(str).str.replace(",","").astype(dtype)

    return df_aux

def assign_frequency_id(
        df:pd.DataFrame, 
        frequency:str
    ) -> pd.DataFrame:

    df_aux = df.copy()
    df_aux["frequency_id"] = periodos_dict[frequency]

    return df_aux

def assign_id(
        df:pd.DataFrame, 
        sort_column:str='application_date'
    ) -> pd.DataFrame:

    df_aux = df.copy()
    df_aux = df_aux.sort_values(by=sort_column).reset_index(drop=True).reset_index().rename(columns={"index":"id"})

    return df_aux

def assign_apr(
        df:pd.DataFrame, 
        dict_aprs: dict=dict_tasas
    ) -> pd.DataFrame:

    df['apr'] = np.where(
        df.application_id.isin(list(manual_apps_pricing.keys())),
        .575,
        np.where(
            df.application_date < "2023-05-26",
            df.tenor.apply(lambda x: dict_aprs[1][x]['ordinal']),
            df.tenor.apply(lambda x: dict_aprs[2][x]['ordinal'])
        )
    )

    return df

def run_loan_table(
        source_version:int, 
        file_path:str|None=None,
        source:str='local',
        vintage:list|None=None,
    ) -> pd.DataFrame:

    if source == "local":
        df = load_data_from_local(source_version,vintage=vintage)
    
    elif source == "drive":
        df = load_data_from_spread(source_version)

    df = format_column_name(df)
    df = format_string_columns(df)
    df = filter_data(df)
    df = transform_timestamp_cols(df, timestamp_columns_dict[source_version])
    df = transform_date_cols(df, date_columns_dict[source_version])
    df = assign_frequency_id(df,'weekly')
    df = rename_cols(df, rename_columns_dict[source_version])
    df = transform_numeric_cols(df, numeric_columns_dict[source_version])
    df = assign_id(df)
    df = format_to_int(df, integer_columns)
    df = filter_columns(df, final_vars_dict[source_version])
    df = assign_apr(df)

    df = df.sort_values(
        by=[
            'name',
            'booking_date'
        ]
    ).drop_duplicates(
        subset='application_id',
        keep='first'
    ).reset_index(drop=True)

    if file_path:
        df.to_csv(
            file_path,
            index=False
        )

    return df


def run_pipeline(
        vintage:int,
        file_path:str|None=None,
        versions:list=[1,2],
        source:str="local",
        verbose:bool=False
):
    
    lst_tables = []
    for version in versions:
        print(version)
        df_aux = run_loan_table(
            version,
            source=source,
            vintage=vintage,
            file_path = f"../../data/core/outputs/temp/loans_temp_{version}"
        )


        df_aux["source"] = version
        
        lst_tables.append(df_aux)
        
        if verbose:
            print(f"Done source: {version}")

    df = pd.concat(lst_tables)

    if file_path:
        df.to_csv(
            file_path,
            index=False
        )

    return df