import re
import pandas as pd
from difflib import SequenceMatcher
from unidecode import unidecode


from boasci.google import Spread
from src.utils.utils import clean_df, clean_date,clean_df_v2
from src.constants.payments import drop_sheets

def read_consolidado(sheet_id, sheet_name, sample=None):
    raw = Spread(sheet_id, mode='r',sheet_name=sheet_name, pull_kwargs={'evaluate_formulas': True})
    data = raw[0].iloc[2:, :36].copy()
    data.columns = raw[0].iloc[1,:36]
    n_clientes = data['Nombre'].dropna().index[-1]
    df = data.iloc[:n_clientes - 1, :].reset_index(drop=True)
    if sample:
        df = df.sample(sample)

    return df

def read_tabs(sheet_id, df):
    clients = {}
    for name in df.Nombre:
        clean_name = re.sub(" +", " ", name).strip().title()
        try: 
            clients[clean_name] =  Spread(sheet_id, mode='r',sheet_name=name, pull_kwargs={'evaluate_formulas': True})[0]

        except Exception as e:
            print(e)
            clients[clean_name] = []

    clean_clients = {}
    for item in clients.keys():
        try: 
            clean_clients[item] = clean_df(clients[item])
            clean_clients[item]['fecha'] = clean_date(clean_clients[item]['fecha'])
            clean_clients[item] = clean_clients[item].assign(
                amortización_capital = clean_clients[item]['amortización_capital'].astype('float'),
                deuda_a_ese_dia_capital = clean_clients[item]['deuda_a_ese_dia_capital'].astype('float'),
                semana = clean_clients[item]['semana'].astype('int'),
                #plazo = clients[item].iloc[4,3].astype('int')
            )
        except:
            clean_clients[item] = []
    return clean_clients

def read_tabs_v2(sheet_id:str,list_clients:list):
    dict_clients = {}
    
    count = 0
    for name in list_clients:
        clean_name = re.sub(" +", " ", name).strip().title()
        count += 1
        try:
            df_client_only = Spread(sheet_id, mode='r',sheet_name=name, pull_kwargs={'evaluate_formulas': True})[0]
            df_client_only = clean_df(
                data=df_client_only
            )
            try:
                df_client_only['fecha'] = clean_date(df_client_only['fecha'])
                df_client_only = df_client_only.assign(
                    amortización_capital = df_client_only['amortización_capital'].astype('float'),
                    deuda_a_ese_dia_capital = df_client_only['deuda_a_ese_dia_capital'].astype('float'),
                    semana = df_client_only['semana'].astype('int'),
                    #plazo = clients[item].iloc[4,3].astype('int')
                )
            except:
                pass
                
            df_client_only['name'] = clean_name
            dict_clients[clean_name] = df_client_only
            
        except Exception as e:
            print(e)          
            print(clean_name)
            dict_clients[clean_name] = []

        except:
            print(clean_name)
            if('El archivo continua aqui' in clean_name):
                print(">>>>>>>>> Finish process <<<<<<<<<<<<<")
                break
            dict_clients[name] = []

        if(count % 100 == 0):
            print(count)
    return dict_clients


def read_tabs_v3(
        filepath:str,
        df_sheets:pd.DataFrame
        ):
    dict_clients = {
        'empty_customers':[]
    }
    
    count = 0
    for index,row in df_sheets.iterrows():
        count += 1
        try:
            df_client_only = pd.read_excel(
                filepath, 
                sheet_name=row['sheet'],#'Abigail Clarissa Fonseca Salvad',
                header=None
            )

            df_client_only.iloc[0]

            df_client_only = clean_df_v2(
                data=df_client_only
            )
            try:
                df_client_only['fecha'] = clean_date(df_client_only['fecha'])
                df_client_only = df_client_only.assign(
                    amortización_capital = df_client_only['amortización_capital'].astype('float'),
                    deuda_a_ese_dia_capital = df_client_only['deuda_a_ese_dia_capital'].astype('float'),
                    semana = df_client_only['semana'].astype('int'),
                    #plazo = clients[item].iloc[4,3].astype('int')
                )
            except:
                pass
                
            df_client_only['application_id'] = row['application_id']
            df_client_only['sheet'] = row['sheet']
            df_client_only['name'] = row['name']
            dict_clients[row['application_id']] = df_client_only
            
        except Exception as e:
            # print(e)          
            # print(row['name'])
            # dict_clients[row['application_id']] = []
            dict_clients['empty_customers'].append(row['application_id'])

        except:
            print(row['name'])
            if('El archivo continua aqui' in row['name']):
                print(">>>>>>>>> Finish process <<<<<<<<<<<<<")
                break
            dict_clients[row['application_id']] = []

        if(count % 50 == 0):
            print(count)
    return dict_clients


def match_sheet_with_name(
    wb,
    df_loans,
    drop_sheets=drop_sheets
):
    sheet_names_1 = [
        x for x in wb.sheetnames if ("Estados de cuen" not in x) and (x not in drop_sheets)
    ]
    clean_sheet_names_1 = [
        unidecode(re.sub(" +", " ", name).title().strip()) for name in sheet_names_1
    ]
    
    df_sheets_1 = pd.DataFrame(
        {
            'sheet':sheet_names_1,
            'clean_sheet':clean_sheet_names_1
        }
    )
    
    df_sheets_1_1 = pd.merge(
        df_sheets_1,
        df_loans[
            [
                'name',
                'application_id'
            ]
        ],
        how='left',
        left_on='clean_sheet',
        right_on='name'
    )
    
    df_sheets_1_2 = pd.merge(
        df_sheets_1_1[
            df_sheets_1_1.name.isna()
        ].drop(columns=['name','application_id']),
        df_loans[
            [
                'name',
                'application_id'
            ]
        ],
        how='cross'
    )

    df_sheets_1_2['similitud'] = df_sheets_1_2.apply(
        lambda row: SequenceMatcher(None, row['clean_sheet'], row['name']).ratio(),
        axis=1

    )
    
    df_sheets_1_2_1 = df_sheets_1_2.sort_values(
        by=[
            'sheet',
            'similitud'
        ]
    ).drop_duplicates(
        subset="sheet",
        keep='last'
    )
    
    df_sheets_1_3 = pd.concat(
        [
            df_sheets_1_2_1,#.drop(columns='similitud'),
            df_sheets_1_1[
                df_sheets_1_1.name.notna()
            ]
        ]
    )
    
    df_sheets_1_3['application_id'] = df_sheets_1_3['application_id'].astype(int)

    return df_sheets_1_3

    