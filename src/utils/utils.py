import numpy as np
import pandas as pd
import calendar
from datetime import datetime


def format_column_name(
        df
    ) -> pd.DataFrame:
    df_aux = df.copy()
    cols = [col.lower().strip().replace(" ", "_").replace("\n", "_").replace("(","").replace(")","").replace("%", "pct") for col in df_aux.columns]
    df_aux.columns = cols 

    return df_aux

def format_to_int(
        df:pd.DataFrame,
        integer_columns
    ) -> pd.DataFrame:
    df_aux = df.copy()
    for var in integer_columns:
        df_aux[var] = df_aux[var].astype(int)
        
    return df_aux

def rename_cols(df, rename_columns) -> pd.DataFrame:
    df_aux = df.copy()
    df_aux = df_aux.rename(
        columns=rename_columns
    )

    return df_aux

def filter_columns(
        df:pd.DataFrame, 
        final_vars:list
    ) -> pd.DataFrame:

    df_aux = df.copy()

    return df_aux[final_vars].copy()

def clean_raw(data):
    nfilas = data.iloc[:,0].dropna().index[-1]
    cols = [col.lower().replace(" ", "_").replace("\n", "_") for col in data.columns]
    
    clean = data.iloc[:nfilas]
    clean.columns = cols
    
    return clean

def asignacion_decil(value, dict_deciles):
    
    for key in dict_deciles.keys():
        
        try:
            if eval(key.format(value=str(value))):
                return dict_deciles[key] 
        except:
            return np.nan
        
def calc_score(z):
    try:
        return 1000*(np.exp(float(z))/(1 + np.exp(float(z))))
    except ValueError:
        return np.nan
    
def get_z_hit(woe_hit):
    w_consultas, w_score, w_edad, w_remanente, w_est_giro, w_est_mun, w_ratio_fin, w_tipo_vivienda, w_estado_civil, w_mop_bancos = woe_hit
    try:
        return 1.0799 + (
            0.4362*w_consultas
        ) + (
            0.56193*w_score
        ) + (
            0.8259*w_edad
        ) + (
            0.5065*w_remanente
        ) + (
            0.9357*w_est_giro
        ) + (
            0.9022*w_est_mun
        ) + (
            0.6503*w_ratio_fin
        ) + (
            0.9480*w_tipo_vivienda
        ) + (
            0.5789*w_estado_civil
        ) + (
            0.4264*w_mop_bancos
        )
    except ValueError:
        return np.nan
    
def to_float(value):
    try:
        return float(value)
    except (ValueError, TypeError):
        return np.nan
    
def clean_cols(todo, cols):
    added = ['']*2 + ['Deuda_a_ese_dia ']*6 + ['']*3 + ['Amortización ']*3 + ['Deuda_despues_pago ']*5
    todo_list = [a+b for a,b in zip(added, cols)]
    return [sub.replace(' ', '_').lower() for sub in todo_list]

def clean_df(data):
    try:
        cleaned = data.iloc[14:,:19]
        cleaned.columns = clean_cols(cleaned, data.iloc[13, :19].values)
        cleaned = cleaned.reset_index(drop=True)
        cleaned = cleaned.iloc[cleaned.fecha.dropna().index,:]
        # cleaned = cleaned.iloc[:cleaned.fecha.dropna().index[-1],:]
    except Exception as e:
        print(e)
        cleaned = data
    return cleaned


def clean_df_v2(data):
    try:
        cleaned = data.iloc[15:,:19]
        cleaned.columns = clean_cols(cleaned, data.iloc[14, :19].values)
        cleaned = cleaned.reset_index(drop=True)
        cleaned = cleaned.iloc[cleaned.fecha.dropna().index,:]
        # cleaned = cleaned.iloc[:cleaned.fecha.dropna().index[-1],:]
        try:
            loan_amount = data.iloc[0,6]
            booking_date = data.iloc[6,3]
            mail = data.iloc[1,9]
            celular = data.iloc[2,9]

            (loan_amount,booking_date,mail,celular)
            cleaned['loan_amount'] = loan_amount
            cleaned['booking_date'] = booking_date
            cleaned['mail'] = mail
            cleaned['celular'] = celular
        except:
            pass
    except Exception as e:
        print(e)
        cleaned = data
    return cleaned

def clean_date(date_series):
    try: 
        date_col = pd.to_datetime(date_series.dropna().map(lambda x: datetime.strptime(str(x).replace(" ", ""),"%m/%d/%y")))
    except ValueError:
        try: 
            date_col = pd.to_datetime(date_series.dropna().map(lambda x: datetime.strptime(str(x).replace(" ", ""),"%d/%m/%y")))
            
        except ValueError:
            try:
                date_col = pd.to_datetime(date_series.dropna().map(lambda x: datetime.strptime(str(x).replace(" ", ""),"%m/%d/%Y")))
            except ValueError:
                date_col = pd.to_datetime(date_series.dropna().map(lambda x: datetime.strptime(str(x).replace(" ", ""),"%d/%m/%Y")), errors='ignore')
    except Exception as e:
        # print(e) 
        date_col = pd.to_datetime(date_series, errors='ignore')
    return date_col

def get_quincena(date):
    if date.day < 16:
        return 1
    else:
        return 2
    
calendar.setfirstweekday(0)

def get_week_of_month(year, month, day):
    x = np.array(calendar.monthcalendar(year, month))
    week_of_month = np.where(x==day)[0][0] + 1
    return(week_of_month)
