import pandas as pd
import numpy as np


from src.finance.constants import deciles_buro,deciles_hit

def read_files(
    dir_core:str="./data/core/outputs/"
):
    
    df_loans = pd.read_csv(
        f"{dir_core}loans.csv"
    )

    df_snap = pd.read_csv(
        f"{dir_core}snapshot_m.csv"
    )

    df_apps = pd.read_csv(
        f"{dir_core}applications.csv",
        sep=";"
    )

    return df_loans, df_snap, df_apps

def transform_init_data(
    df_apps:pd.DataFrame,
    df_snap:pd.DataFrame,
    df_loans:pd.DataFrame
):
    try:
        df_loans = df_loans.drop(columns="score_generico")
    except:
        pass
    
    df_apps = df_apps.sort_values(
        by=[
            "id",
            "score_generico"
        ]
    ).drop_duplicates(keep="first",subset="id")

    df_snap['bucket'] = pd.cut(
        df_snap.dpd.fillna(0),
        bins=[-1,1,29,59, 89,1000],
        labels=[
            "AC",
            "1-29",
            "30-59",
            "60-89",
            "90+",
        ]
    )

    df_base = pd.merge(
        df_loans,
        df_apps[
            [
                'id',
                'score_generico',
                'score_hit',
                'score_no_hit'
            ]
        ].rename(
            columns={
                "id":"application_id"
            }
        ),
        how='left',
        on="application_id"
    )

    df_base["na_score"] = df_base.score_generico.isna() | df_base.score_hit.isna()
    df_base["score_no_hit"] = df_base["score_no_hit"].fillna(841)
    df_base["score_used"] = np.where(
        df_base.score_generico >= 617,
        "generico",
        np.where(
            df_base.score_hit >= 767,
            "hit",
            "no_hit"
        )
    )

    return df_base

def get_decil(
    df_base:pd.DataFrame
):
    df_base["decil_generico"] = pd.cut(
        df_base.score_generico,
        bins=[
            -1,
            400,
            639,
            654,
            667,
            674,
            682,
            690,
            698,
            704,
            712,
            2000
        ],
        labels=[
            0,        
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10
        ]
    )

    df_base["decil_hit"] = pd.cut(
        df_base.score_hit,
        bins=[
            -1,
            447,
            546,
            626,
            700,
            766,
            831,
            883,
            923,
            949,
            2000
        ],
        labels=[
            1,     
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10
        ]
    )

    df_base["decil_no_hit"] = pd.cut(
        df_base.score_no_hit,
        bins=[
            -1,
            528,
            636,
            718,
            785,
            841,
            891,
            930,
            960,
            981,
            2000,
        ],
        labels=[
            1,     
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10
        ]
    )

    df_base["prob_npl_generico"] = df_base.decil_generico.map(
        deciles_buro
    )

    df_base["prob_npl_hit"] = df_base.decil_hit.map(
        deciles_hit
    )

    df_base["prob_npl_no_hit"] = df_base.decil_no_hit.map(
        deciles_hit
    )

    df_base["prob_npl"] = np.where(
        df_base.score_used == "generico",
        df_base["prob_npl_generico"],
        np.where(
            df_base.score_used == "hit",
            df_base["prob_npl_hit"],
            df_base["prob_npl_no_hit"]
        )
    )

    return df_base

def get_prov_table(
    vintage:str,
    df_snap:pd.DataFrame
):
    df_aux = df_snap[
        df_snap.month_end == vintage
    ][
        [
            "application_id",
            "booking_date",
            "bucket",
            "loan_amount",
            "principal_paid"
        ]
    ].assign(
        balance=lambda x: x.loan_amount - x.principal_paid.fillna(0)
    ).pivot(
        columns="bucket",
        index=["application_id","booking_date","loan_amount"],
        values="balance"
    ).reset_index().drop(
        columns="bucket",
        errors="ignore"
    ).copy().reset_index(drop=True)
    
    return df_aux

def run_pipeline(
    end_date:str,
    ini_date:str="2023-01-01",
    file_path:str|None=None,
):
    
    df_loans, df_snap, df_apps = read_files()
    df_base = transform_init_data(df_apps,df_snap,df_loans)
    df_base = get_decil(df_base)

    writer = pd.ExcelWriter(f"{file_path}/provisiones_motos.xlsx", engine="xlsxwriter")

    for x in pd.date_range(start=ini_date, end=end_date, freq="M"):
        vintage = x.strftime('%Y-%m-%d')
        df_aux_1 = get_prov_table(vintage,df_snap)
        df_aux = pd.concat(
            [
                df_aux_1,
                df_loans[
                    (pd.to_datetime(df_loans.booking_date).dt.normalize() <= vintage)
                    & (~df_loans.application_id.isin(df_aux_1.application_id))
                ][
                    [
                        "application_id",
                        "booking_date",
                        "loan_amount"
                    ]
                ].assign(
                    AC=lambda x: x.loan_amount
                )
            ]
        )

        #Create Buckest if doesnt exists
        for bucket_col in ['1-29','30-59','60-89','90+']:
            if bucket_col not in df_aux.columns:
                # Add a new column filled with NaN values
                df_aux[bucket_col] = float('nan')

        # Create a new column 'prob_npl' based on the conditions
        df_aux.loc[df_aux['AC'].notna(), 'prob_npl']    = 0.198
        df_aux.loc[df_aux['1-29'].notna(), 'prob_npl']  = 0.341
        df_aux.loc[df_aux['30-59'].notna(), 'prob_npl'] = 0.502
        df_aux.loc[df_aux['60-89'].notna(), 'prob_npl'] = 0.670
        df_aux.loc[df_aux['90+'].notna(), 'prob_npl']   = 0.806  

        df_aux["preprovision"] = df_aux.prob_npl*df_aux[["AC", "1-29","30-59","60-89","90+"]].sum(axis=1)
        df_aux["f1"] = .1*df_aux["1-29"]*df_aux.prob_npl
        df_aux["f2"] = .5*df_aux["30-59"]*df_aux.prob_npl
        df_aux["f3"] = .7*df_aux["60-89"]*df_aux.prob_npl
        df_aux["f4"] = df_aux["90+"]*df_aux.prob_npl
        df_aux.reset_index(inplace=True, drop=True)
        df_aux.to_excel(writer, sheet_name=vintage)
        print(df_aux.info())

    df_aux
    writer.close()