data_sources_google = {
    1:{
        'id':"1mt0DcAb_ATFxkqEjpeMOy-gdnMpFt0OcPqqbSaOVg1E"
    }
}

str_columns_1 = [
    'estado_final',
    'causal_rechazo/aceptación',
]

str_columns_2 = [
    'ref_moto_solicitada',
    'ref_moto_aprobada',
]

date_columns_dict = {
    1:[
        "fecha_aplicación",
    ]
}

integer_columns = [
    'application_id',
    'tenor'
]

rename_columns_dict = {
    1:{
        "id_cliente":"id",
        "ref_moto_solicitada": "requested_product",
        "ref_moto_aprobada": "approved_product",
        "plazos": "tenor",
        "pct_enganche": "pct_downpayment",
        "valor_enganche": "downpayment",
        "score_genérico_buró": "score_generico",
        "fecha_aplicación": "created_date",
        "remanente": "payment_capacity"
    }
}

date_columns_dict = {
    1:[
        "fecha_aplicación",
    ]
}

final_vars_dict = {
    1:[
    'id',
    'score_generico',
    'score_hit',
    'score_no_hit',
    'payment_capacity',
    'requested_product',
    'approved_product',
    'tenor',
    'pct_downpayment',
    'downpayment',
    'created_date',
    'store_id',
    'status_id',
    'booked',
    'completed',
    'risk_decision',
    'rejection_cause_risk',
    'rejection_cause_kyc',
    'rejection_cause',
    'frequency',
    'approved'
    ]
}

str_cols = [
    'requested_product',
    'approved_product',
    'rejection_cause_risk',
    'rejection_cause_kyc',
    'rejection_cause'
]

numeric_cols = [
    'id',
    'score_generico',
    'score_hit',
    'score_no_hit',
    'payment_capacity',
    'tenor',
    'pct_downpayment',
    'downpayment',
    'store_id',
    'status_id',
    'booked',
    'completed',
    'risk_decision',
    'frequency',
    'approved'  
]

data_types = {
    'id': 'int64',
    'score_generico': 'float64',
    'score_hit': 'float64',
    'score_no_hit': 'float64',
    'payment_capacity' : 'float64',
    'requested_product': 'str',
    'approved_product': 'str',
    'tenor': 'Int64',
    'pct_downpayment': 'float64',
    'downpayment': 'float64',
    'created_date': 'datetime64[ns]',
    'store_id': 'Int64',
    'status_id': 'Int64',
    'booked': 'Int64',
    'completed': 'Int64',
    'risk_decision': 'Int64',
    'rejection_cause_risk': 'str',
    'rejection_cause_kyc': 'str',
    'rejection_cause': 'str',
    'frequency': 'Int64',
    'approved': 'Int64'
}