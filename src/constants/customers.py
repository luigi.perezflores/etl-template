data_sources_google = {
    1:{
        'id':"1mt0DcAb_ATFxkqEjpeMOy-gdnMpFt0OcPqqbSaOVg1E"
    }
}

rename_columns_dict = {
    1:{
        "id_cliente": "application_id",
        "sexo": "sex",
        "numero_celular": "cellphone",
    }
}

final_vars_dict = {
    1:[
    "application_id", 
    "nombre", 
    "apellido", 
    "rfc", 
    "email", 
    "cellphone", 
    "sex"
    ]
}

str_cols = [
    "nombre", 
    "apellido", 
    "rfc", 
    "email",
    "sex"  
]

numeric_cols = [
    "application_id", 
    "cellphone",
]

data_types = {
    "application_id": "int64", 
    "nombre": "str",
    "apellido": "str", 
    "rfc": "str",
    "email": "str",
    "cellphone": "int64", 
    "sex": "str"
}