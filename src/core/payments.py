import pandas as pd
import math
import datetime
import numpy_financial as npf
import warnings

from src.core.calculator.v3 import calculator_v3
from src.core.calculator.utils import transform_apr_to_period
from src.core.constants import (
    cols_to_date,
    init_date,
)

warnings.filterwarnings('ignore')


def get_end_date(
        vintage:int|str
    ) -> str:

    new_vintage = vintage + 1 if int(str(vintage)[-2:]) == 1 else vintage
    end_date = (pd.to_datetime(new_vintage, format="%Y%m%d") + pd.offsets.BMonthBegin(1)).strftime("%Y-%m-%d")

    return end_date

def read_payments_files(
    vintage:int|str,
    dir_payments:str="./data/core/raw/payments/",
    warnings:bool=False
) -> pd.DataFrame:
    
    end_date = get_end_date(vintage)

    lst_tables = []
    vintages_not_found = []
    for x in pd.date_range(start=init_date, end=end_date, freq="M"):
        try:
            df_aux = pd.read_excel(
                f"{dir_payments}{vintage}/created_in_{x.strftime('%Y%m')}.xlsx"
            )
            lst_tables.append(df_aux)

        except FileNotFoundError:
            vintages_not_found.append(x.strftime('%Y%m'))

    if warnings & (len(vintages_not_found) > 0):
        print(f"Vintages no encontrados: {vintages_not_found}")

    df = pd.concat(lst_tables)

    return df

def tranform_data(
    df,
    cols_to_date
) -> pd.DataFrame:

    for col in cols_to_date["payments"]:
        df[col] = pd.to_datetime(df[col], errors='coerce')

    df = df.sort_values(
        by = [
            "application_id",
            "created_date"
        ]
    )

    df['payment_number'] = df.groupby(['application_id']).cumcount()+1

    return df

def amortization_table(
    period_days:int, 
    term:int,
    loan_amount:float, 
    initial_date:str,
    apr:float,
    base_period:int=360,
    effective_rate:bool=True,
    verbose:bool=False,
    iva:float=0.16
):
    
    m = base_period/period_days
    
    # Calculate interest rate
    effective_rate = transform_apr_to_period(apr,m,False) 
    effective_rate_iva = effective_rate * (1+iva)
    annual_effective_rate = (1 + effective_rate) ** (360/7) - 1
    
    if verbose:
        print(f"Interest rate for period {m:.2f}: {effective_rate:.5%}")
        print(f"Interest rate with iva: {effective_rate_iva:.5%}")
        print(f"Annual interest rate: {annual_effective_rate:.5%}")

    # effective_rate_iva = effective_rate * 1.16
    pmt = math.ceil(npf.pmt(effective_rate_iva, term, -loan_amount, 0, 0)*10)/10
    
    # dataframe creation
    df_amortization = pd.DataFrame(
        {
            'payment_number':range(1,term+1,1)
        }
    )
    
    df_amortization['payment_date'] = df_amortization.apply(
        lambda row: pd.to_datetime(initial_date) + datetime.timedelta(days=int((row['payment_number'])*period_days)),
        axis=1
    )
    
    df_amortization['payment_amount'] = pmt

    df_amortization['principal'] = df_amortization.apply(
        lambda row: npf.ppmt(
            effective_rate_iva,
            row['payment_number'],
            term,
            -loan_amount,
        ),
        axis=1
    )

    df_amortization['interest'] = (pmt - df_amortization['principal']) / (1 + iva)
    df_amortization['iva'] = df_amortization['interest'] * iva

    return df_amortization

def get_amortization_table(
    df_loans:pd.DataFrame,
    period_days:int=7
):
    lst_calculators = []
    for index,row in df_loans.iterrows():
        df_amortization_aux = amortization_table(
            period_days,
            term=int(row['tenor']), 
            loan_amount = row['loan_amount'], 
            initial_date = row['booking_date'], 
            effective_rate=False,
            apr=row["apr"]
        )
        df_amortization_aux['application_id'] = row['application_id']
        lst_calculators.append(df_amortization_aux)
        
    df_amortizacion = pd.concat(lst_calculators)
    df_amortizacion['payment_amount_cumm'] = df_amortizacion.payment_number * df_amortizacion.payment_amount
    df_amortizacion["payment_date"] = df_amortizacion["payment_date"].dt.date

    return df_amortizacion

def get_duplicated_payments(
    df,
    subset:list=["application_id","created_date","payment_amount"]
):
    df_duplicates = df[
        df[subset].duplicated(keep=False)
    ].sort_values(by=subset)

    df_duplicates.to_csv(
        "./data/tests/duplicated_payments.csv",
        index=False
    )

def run_pipeline(
    vintage:int,
    file_path:str|None=None,
    warnings:bool=False,
    verbose:bool=False
):
    df_payments = read_payments_files(vintage, warnings=warnings)
    df_payments = tranform_data(df_payments,cols_to_date)
    
    get_duplicated_payments(df_payments)
    
    current_date = pd.to_datetime(vintage, format="%Y%m%d").strftime("%Y-%m-%d")

    # read files
    df_loans = pd.read_csv(f"{file_path}loans.csv")
    df_amortization = get_amortization_table(df_loans)


    df_payments_temp = df_payments = pd.merge(
        df_payments,
        df_loans[
            [
                "application_id",
                "booking_date",
                "rfc",
                "name",
            ]
        ],
        how="left",
        on="application_id"
    )
    
    df_payments_result, df_amortizacion_result = calculator_v3(
        current_date,
        df_amortization,
        df_payments_temp
    )
    if verbose:
        print(df_amortizacion_result.info())
        print(df_payments_result.info())

    if file_path:
        df_payments.to_csv(
            f"{file_path}temp/payments_temp.csv",
            index=False
        )

        df_amortization.to_csv(
            f"{file_path}temp/amortizacion_waldos_motos_temp.csv",
            index=False
        )

        df_payments_result.to_csv(
            f"{file_path}payments_with_local_calculator_v3.csv",
            index=False
        )

        df_amortizacion_result.to_csv(
            f"{file_path}amortizacion_waldos_motos.csv",
            index=False
        )

        df_payments_result[
            [
                "application_id",
                "name",
                "payment_number",
                "payment_amount",
                "principal",
                "interest",
                "iva",
                "created_date",
                "dpd",
                "bucket",
            ]
        ].rename(
            columns={
                "principal":"capital",
            }
        ).to_csv(
            f"{file_path}payments.csv",
            index=False
        )

    print("- Payments ha terminado")
