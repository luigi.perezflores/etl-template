from google.spread import Spread
import pandas as pd 
from src.core.constants import data_sources_google, data_sources_google_apps
import zipfile
import os
import shutil
import sys


def transform_apr_to_period(apr, m, effective_rate=True):
    new_rate = (1+apr)**(1/m) - 1 if effective_rate else apr/m
    return new_rate

def load_data_from_spread(google_sheet_id: str, database: dict) -> pd.DataFrame:
    """
    Carga datos desde una hoja de cálculo de Google en un DataFrame de pandas.

    Esta función utiliza la librería 'google.spread' para acceder a una hoja de cálculo
    de Google y cargar los datos en un DataFrame de pandas. Se pueden aplicar opciones
    de configuración, como saltar filas iniciales y evaluar fórmulas al cargar los datos.

    Args:
        google_sheet_id (str): El ID de la hoja de cálculo de Google que se va a acceder.
        database (dict): Un diccionario que contiene opciones de configuración para la carga
                         de datos, como el nombre de la hoja y las filas a omitir.

    Returns:
        pandas.DataFrame: Un DataFrame que contiene los datos cargados desde la hoja de cálculo.

    Example:
        google_sheet_id = "your_sheet_id_here"
        database = {
            "sheet_name": "Sheet1",
            "skip_rows": 2
        }
        df = load_data_from_spread(google_sheet_id, database)
    """
    # Acceder a la hoja de cálculo de Google y cargar los datos en un DataFrame
    df = Spread(
        url=google_sheet_id,
        mode="r",
        pull_kwargs={
            "evaluate_formulas": True,
            "skiprows": database.get("skip_rows", 0),
        },
        sheet_name=database.get("sheet_name", ""),
    )[database.get("sheet_name", "")]

    # Eliminar filas con valores faltantes en el DataFrame
    df = df.dropna(axis=0, how="all")

    return df


def load_data_from_local(source_version: int, vintage: int, database: dict) -> pd.DataFrame:
    """
    Carga datos desde un archivo local en un DataFrame de pandas.

    Esta función carga los datos desde un archivo local en formato Excel en un DataFrame
    de pandas. Se deben proporcionar la versión de la fuente de datos y la 'vintage' del
    archivo que se desea cargar. Las rutas a los archivos y el nombre de la hoja se obtienen
    a partir del diccionario 'data_sources_google_apps'.

    Args:
        source_version (int): La versión de la fuente de datos de la que se desea cargar los datos.
        vintage (int): El año o identificador de la versión específica del archivo a cargar.

    Returns:
        pandas.DataFrame: Un DataFrame que contiene los datos cargados desde el archivo local.

    Example:
        source_version = 1
        vintage = 2023
        database = data_sources_google_apps["segunda_fuente"]["databases"][
        "mesa_control_comprobante_nomina"]
        df = load_data_from_local(database, source_version, vintage)
    """
    # Obtener la ruta y el nombre de la hoja de la fuente de datos desde el diccionario
    file_path = source_version["file"].format(vintage=vintage)
    skip_rows= database.get("skip_rows", 0)
    sheet_name = database.get("sheet_name", "")
    if len(sheet_name)>31:
        sheet_name = sheet_name[:31]
        
    # Leer los datos desde el archivo Excel y copiarlos en un DataFrame
    df = pd.read_excel(file_path, sheet_name=sheet_name,skiprows=skip_rows).copy()

    return df

def get_data(source_version, db,vintage,source):
    if source == 'online':
        df = load_data_from_spread(data_sources_google[source_version]['id'], db)
    if source == 'local':
        df = load_data_from_local(data_sources_google[source_version], vintage, db) 
    return df



def process_payments_dir(vintage):
    dir_payments = './data/core/raw/payments/'

    if os.path.exists(dir_payments+str(vintage)):
        print(f"No se extraerá el ZIP, el directorio {dir_payments+str(vintage)} ya existe.")
        return
    archivos_zip = [archivo for archivo in os.listdir(dir_payments) if archivo.endswith('.zip')]

    if len(archivos_zip) < 1:    
        print(f"Error: No hay ningún archivo ZIP en el directorio {dir_payments}")
        sys.exit(1)
    elif len(archivos_zip) > 1:
        try:
            archivo_zip = [archivo for archivo in archivos_zip if str(vintage) in archivo][0]
        except: 
            print(f"Error: No hay ningún archivo ZIP con la fecha seleccionada en el directorio {dir_payments}")
            sys.exit(1)
    else:
        archivo_zip = archivos_zip[0]       

    # Paso 1: Extraer el archivo ZIP en el mismo directorio
    with zipfile.ZipFile(os.path.join(dir_payments, archivo_zip), 'r') as zip_ref:
        zip_ref.extractall(os.path.join(dir_payments, 'temp'))

    # Paso 2: Renombrar la carpeta "Payments" a "vintage"
    old_payments_path = os.path.join(dir_payments, 'temp', 'Payments')
    new_payments_path = os.path.join(dir_payments, 'temp', str(vintage))
    os.rename(old_payments_path, new_payments_path)

    # Paso 3: Mover la carpeta renombrada al directorio dir_payments
    shutil.move(new_payments_path, dir_payments)

    # Limpiar directorio temporal
    shutil.rmtree(os.path.join(dir_payments, 'temp'))
    print('Archivo de payments extraído')