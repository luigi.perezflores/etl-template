import pandas as pd
import numpy as np

def calculate_bucket(
    dpd_serie
) -> pd.Series:

    bucket_serie = pd.cut(
        dpd_serie.fillna(0),
        bins=[-1,1,29,59, 89,119,180,1000],
        labels=[
            "00.Current",
            "0.1-29",
            "1.30-59",
            "2.60-89",
            "3.90-119",
            "4.120-179",
            "5.180+"
        ]
    )

    return bucket_serie

def calculate_amortization_table(
    current_date,
    df_amortizacion,
    df_completed_dates
) -> pd.DataFrame:
    df_amortization_v2 = pd.merge(
        df_amortizacion,
        df_completed_dates,
        how="left",
        on=["application_id","payment_number"]
    )

    lst_date_vars = [
        # "completed_date",
        "payment_date"
    ]

    for var in lst_date_vars:
        df_amortization_v2[var] = pd.to_datetime(df_amortization_v2[var]) 

    df_amortization_v2["completed_date_aux"] = pd.to_datetime(
        np.where(
            df_amortization_v2.completed_date.notna(),
            df_amortization_v2.completed_date.dt.strftime("%Y-%m-%d"),
            np.where(
                df_amortization_v2.payment_date <= current_date,
                current_date,
                np.nan
            )
        ), 
        errors="coerce"
    )

    df_amortization_v2["dpd"] = np.maximum(
        (df_amortization_v2["completed_date_aux"] - df_amortization_v2.payment_date).dt.days,
        0
    )

    df_amortization_v2 = df_amortization_v2.drop(
        columns = [
            "completed_date_aux"
        ]
    )

    df_amortization_v2['bucket'] = calculate_bucket(df_amortization_v2.dpd)

    return df_amortization_v2


def transform_apr_to_period(apr, m, effective_rate=True):
    new_rate = (1+apr)**(1/m) - 1 if effective_rate else apr/m
    
    return new_rate