import pandas as pd
from src.core.calculator.utils import calculate_bucket, calculate_amortization_table

def calculator_v3(
    current_date:str,
    df_amortizacion:pd.DataFrame,
    df_payments:pd.DataFrame,
    tasa_iva:float=0.16
):
    lst_results = []
    lst_completed_dfs = []
    for application_id in df_amortizacion.application_id.unique():
        
        # Se filtran las tablas de pagos y quotas para cada apliación
        df_amortizacion_aux = df_amortizacion[
            df_amortizacion.application_id == application_id
        ].copy()
        
        df_payments_aux = df_payments[
            df_payments.application_id == application_id
        ].copy()
        
        # Se identifica la cuota maxima
        quota_max = df_amortizacion_aux.payment_number.max()

        quota_number = 1
        payments_made = []
        principals_made = []
        interests_made = []
        ivas_made = []
        # Se itera para cada pago
        created_dates = []
        payment_numbers = []
        booking_dates = []
        modified_dates = []
        payment_application_dates = []
        lst_total_quotes = []
        last_quotes = []
        flag_next = True
        pago_number = 1    
        completed_dates = []
        completed_quotes = []
        total_dpds = []
        payment_method = []
        account = []
        
        
        # Se itera para cada pago hecho por el cliente
        for index, row in df_payments_aux.iterrows():
            payment_amount = row['payment_amount']
            payment_date = row["created_date"]
            current_dpd = 0
            payment_amount_made = 0
            principal_made = 0
            interest_made = 0
            iva_made = 0  
            total_quotes = []
            
            # Se itera hasta que el pago finaliza
            while payment_amount > 0:
                
                # Tira el proceso si la cuota es mayor a lo pactado originalmente
                if quota_max < quota_number:
                    break
                
                total_quotes.append(quota_number)
                
                if flag_next:
                    due_payment_amount, due_principal, due_interest, due_iva, quota_date = df_amortizacion_aux[
                        df_amortizacion_aux.payment_number == quota_number
                    ][['payment_amount','principal','interest','iva', 'payment_date']].iloc[0]             
                    
                current_dpd = max(current_dpd,(pd.to_datetime(payment_date) - pd.to_datetime(quota_date)).days)
                

                # pago de la cuota mayor al pago realizado
                if due_payment_amount > payment_amount:
                    
                    if  payment_amount < due_interest * (1 + tasa_iva):
                        pct_principal = 0
                        pct_interest = 1 / (1+tasa_iva)
                        pct_iva = 1 - pct_interest
                        
                    else:
                        pct_interest = due_interest / payment_amount
                        pct_iva = due_iva / payment_amount
                        pct_principal = 1-pct_interest-pct_iva

                    payment_amount_made += payment_amount
                    principal_made += payment_amount * pct_principal
                    interest_made += payment_amount * pct_interest
                    iva_made += payment_amount * pct_iva
                    flag_next = False
                    due_payment_amount -= payment_amount
                    due_principal -= payment_amount * pct_principal
                    due_interest -= payment_amount * pct_interest
                    due_iva -= payment_amount * pct_iva
                    
                    payment_amount = 0

                # pago de la cuota menor al pago realizado   
                else:
                    completed_dates.append(payment_date)
                    completed_quotes.append(quota_number)
                    flag_next = True
                    quota_number += 1 
                    payment_amount_made += due_payment_amount
                    principal_made += due_principal
                    interest_made += due_interest
                    iva_made += due_iva
                    payment_amount -= due_payment_amount

            pago_number += 1
            payments_made.append(payment_amount_made)
            principals_made.append(principal_made)
            interests_made.append(interest_made)
            ivas_made.append(iva_made)
            total_dpds.append(current_dpd)
            created_dates.append(row["created_date"])
            payment_numbers.append(row["payment_number"])
            booking_dates.append(row["booking_date"])
            modified_dates.append(row["modified_date"])
            payment_application_dates.append(row["payment_application_date"])
            last_quotes.append(quota_number)
            lst_total_quotes.append(list(set(total_quotes)))
            account.append(row['account'])
            payment_method.append(row['payment_method'])

        df_result_aux = pd.DataFrame(
            {
                "payment_amount":payments_made,
                "principal":principals_made,
                "interest":interests_made,
                "iva":ivas_made,
                "created_date":created_dates,
                "payment_number":payment_numbers,
                "rfc":row["rfc"],
                "name":row["name"],
                "booking_date":booking_dates,
                "last_quote":last_quotes,
                "total_quotes":lst_total_quotes,
                "modified_date":modified_dates,
                "payment_application_date":payment_application_dates,
                "dpd":total_dpds,
                "payment_method": payment_method,
                "account":account
            }
        )
        
        df_completed_dates_aux = pd.DataFrame(
            {
                "payment_number":completed_quotes,
                "completed_date":completed_dates
            }
        )

        df_completed_dates_aux['application_id'] = application_id
        df_result_aux['application_id'] = application_id
        
        lst_results.append(df_result_aux)
        lst_completed_dfs.append(df_completed_dates_aux)

    df_payments_result = pd.concat(lst_results)
    df_completed_dates = pd.concat(lst_completed_dfs)

    df_payments_result = df_payments_result[
        [
            "application_id",
            "payment_number",
            "created_date",
            "payment_amount",
            "account",
            "principal",
            "interest",
            "iva",
            "rfc",
            "name",
            "booking_date",
            "last_quote",
            "total_quotes",
            "modified_date",
            "payment_application_date",
            "dpd",
            "payment_method"
            
        ]
    ].reset_index(drop=True)

    df_payments_result["payment_number"] = df_payments_result["payment_number"].astype(int)
    df_payments_result['bucket'] = calculate_bucket(df_payments_result.dpd)

    df_amortizacion_result = calculate_amortization_table(
        current_date,
        df_amortizacion,
        df_completed_dates
    )

    return df_payments_result, df_amortizacion_result