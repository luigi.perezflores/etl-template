import pandas as pd
import numpy as np
from datetime import datetime, timedelta

from src.core.calculator.utils import calculate_bucket

def read_files(
    dir_core:str="./data/core/outputs/"
):
    
    df_loans = pd.read_csv(
        f"{dir_core}loans.csv"
    )

    df_payments = pd.read_csv(
         f"{dir_core}payments.csv"
    )

    df_amt = pd.read_csv(
        f"{dir_core}amortizacion_waldos_motos.csv"
    )

    df_snapshot_m = pd.read_csv(
        f"{dir_core}snapshot_m.csv"
    )

    return df_loans, df_payments, df_amt, df_snapshot_m

def transform_tables(
    df_payments:pd.DataFrame,
    df_loans:pd.DataFrame,
    df_amt:pd.DataFrame

):
    
    df_payments = pd.merge(
        df_payments,
        df_loans[
            [
                "application_id",
                "booking_date"
            ]
        ],
        how="left",
        on="application_id"
    )

    df_amt = pd.merge(
        df_amt,
        df_loans[
            [
                "application_id",
                "booking_date"
            ]
        ],
        how="left",
        on="application_id"
    )

    return df_payments,df_amt

def get_date_ranges(min_date: str) -> list:
    # Convierte la cadena de fecha a un objeto datetime
    fecha_obj = datetime.strptime(min_date, '%Y-%m-%d')

    # Calcula el día de la semana (0 es lunes, 6 es domingo)
    dia_semana = fecha_obj.weekday()

    # Calcula la fecha del lunes de la semana actual
    lunes_semana_actual = fecha_obj - timedelta(days=dia_semana)

    # Lista para almacenar las fechas semanales
    fechas_semanales = []

    # Itera desde el lunes de la semana actual hasta hoy
    while lunes_semana_actual <= datetime.now() + timedelta(days=7):
        # Añade el rango de fechas de la semana actual a la lista
        fechas_semanales.append((
            lunes_semana_actual.strftime('%Y-%m-%d'),
            (lunes_semana_actual + timedelta(days=6)).strftime('%Y-%m-%d')
        ))

        # Avanza al próximo lunes
        lunes_semana_actual += timedelta(weeks=1)

    return fechas_semanales

def get_reporting_tables(
    df_amt,
    df_payments,
    min_date:str="2022-04-01"

):
    
    lst_tables = []
    lst_matrix = []
    date_ranges = get_date_ranges(min_date)

    for init_date, end_date in date_ranges:
        # Tabla de amortización
        df_aux = df_amt[
            (df_amt.payment_date < end_date)
            & (
                ( (df_amt.completed_date < end_date) & (df_amt.completed_date >= init_date))
                | (df_amt.completed_date.isna())
            )
        ].drop(
            columns=["dpd","bucket"]
        )
        
        # Tabla de pagos
        df_payments_aux = df_payments[
            (df_payments.created_date < end_date) & (df_payments.created_date >= init_date)
        ].copy()
        
        df_payments_aux['vintage'] = pd.to_datetime(init_date).strftime("%Y-%m-%d")
        
        
        df_payments_aux['originated'] = np.where(
            df_payments_aux.booking_date < init_date,
            "past",
            "new"
        )
        
        df_aux['originated'] = np.where(
            df_aux.booking_date < init_date,
            "past",
            "new"
        )   
        
        df_payments_aux = df_payments_aux.groupby(
            [
                "vintage",
                "application_id",
                "bucket",
                "originated"
            ],
            as_index=False
        ).agg(
            payment_amount_paid = ("payment_amount",sum)
        )
        
        df_aux["dpd"] = np.maximum(
            (pd.to_datetime(init_date) - pd.to_datetime(df_aux["payment_date"])).dt.days,
            0
        )
        
        df_aux["dpd_post"] = np.where(
            (df_aux.completed_date < end_date) & (df_aux.completed_date.notna()), 
            0,
            np.maximum(
                (pd.to_datetime(end_date) - pd.to_datetime(df_aux["payment_date"])).dt.days - 1,
                0
            )
        )
        
        df_aux['bucket'] = calculate_bucket(df_aux.dpd).astype(str)
        df_aux['bucket_post'] = calculate_bucket(df_aux.dpd_post).astype(str)
        df_aux['vintage'] = pd.to_datetime(init_date).strftime("%Y-%m-%d")
        df_aux = df_aux.reset_index(drop=True)
        
        df_aux_agg = df_aux.groupby(
            [
                "vintage",
                "application_id",
                "bucket",
                "originated",
            ],
            as_index=False
        ).agg(
            payment_amount = ("payment_amount",sum),
            number_of_quotes = ("application_id","count"),
            number_of_loans = ("application_id","nunique")
        )
        
        df_aux_agg = pd.merge(
            df_aux_agg,
            df_payments_aux,
            how="outer",
            on=["vintage","application_id","bucket","originated"]
        )

        mapeo = {
            "00.Current": -1,
            "0.1-29": 0,
            "1.30-59": 1,
            "2.60-89": 2,
            "3.90-119": 3,
            "4.120-179": 4,
            "5.180+": 5
        }

        df_aux_agg['bucket'] = df_aux_agg['bucket'].replace(mapeo)

        df_aux_agg = df_aux_agg.sort_values(
            by=[
                "vintage",
                "application_id",
                "originated",
                "bucket"
            ], 
            ascending=[
                True, 
                True,
                True,
                False
                ]
        ).groupby(
            [
                "vintage",
                "application_id",
                "originated",
            ],
            as_index=False
        ).agg(
            bucket = ("bucket","first"),
            payment_amount = ("payment_amount",sum),
            payment_amount_paid = ("payment_amount_paid",sum),
            number_of_quotes = ("application_id","count"),
            number_of_loans = ("application_id","nunique")
        )

        mapeo_inv = {
            -1: "00.Current",
            0: "0.1-29",
            1: "1.30-59",
            2: "2.60-89",
            3: "3.90-119",
            4: "4.120-179",
            5: "5.180+"
        }

        df_aux_agg['bucket'] = df_aux_agg['bucket'].replace(mapeo_inv)
        
        df_aux_matrix = df_aux.pivot_table(
            index=["vintage","bucket"],
            columns="bucket_post",
            values="payment_amount",
            aggfunc=sum
        ).sort_index()
        
        lst_tables.append(df_aux_agg)
        lst_matrix.append(df_aux_matrix)
        
        df_result = pd.concat(lst_tables)
        df_result_matrix = pd.concat(lst_matrix)

    return df_result, df_result_matrix

def get_default_proba(df_, id, status, vintage, t):

    lst_vintage=[]
    lst_applications=[]
    lst_defaulters=[]
    lst_proba_default_C=[]
    lst_proba_default_0=[]
    lst_proba_default_1=[]
    lst_proba_default_2=[]
    lst_proba_default_3=[]

    def calculate_proba_default(delinquency, defaulters):
        try:
            return len(set(delinquency).intersection(defaulters)) / len(delinquency)
        except ZeroDivisionError:
            return 0

    vintage_t = df_.groupby(vintage).agg({t:"max"})
    lst_vintages = vintage_t.where(vintage_t >= 6).dropna().index.tolist()
    df_6m = df_[df_[vintage].isin(lst_vintages)].copy()

    for vintage_i in df_6m.vintage.unique():

        df = df_6m[df_6m[vintage]==vintage_i].copy()
        
        defaulters = list(set(df[df[status].isin(['4.120-179','5.180+'])][id]))

        current = list(set(df[df[status]=="00.Current"][id]))
        delinquency0 = list(set(df[df[status]=="0.1-29"][id]))
        delinquency1 = list(set(df[df[status]=="1.30-59"][id]))
        delinquency2 = list(set(df[df[status]=="2.60-89"][id]))
        delinquency3 = list(set(df[df[status]=="3.90-119"][id]))

        proba_default_C = calculate_proba_default(current, defaulters)
        proba_default_0 = calculate_proba_default(delinquency0, defaulters)
        proba_default_1 = calculate_proba_default(delinquency1, defaulters)
        proba_default_2 = calculate_proba_default(delinquency2, defaulters)
        proba_default_3 = calculate_proba_default(delinquency3, defaulters)

        lst_vintage.append(vintage_i)
        lst_applications.append(df[id].nunique())
        lst_defaulters.append(len(defaulters))
        lst_proba_default_C.append(proba_default_C)
        lst_proba_default_0.append(proba_default_0)
        lst_proba_default_1.append(proba_default_1)
        lst_proba_default_2.append(proba_default_2)
        lst_proba_default_3.append(proba_default_3)

    #Totals
    df=df_6m.copy()
    defaulters = list(set(df[df[status].isin(['4.120-179','5.180+'])][id]))

    current = list(set(df[df[status]=="00.Current"][id]))
    delinquency0 = list(set(df[df[status]=="0.1-29"][id]))
    delinquency1 = list(set(df[df[status]=="1.30-59"][id]))
    delinquency2 = list(set(df[df[status]=="2.60-89"][id]))
    delinquency3 = list(set(df[df[status]=="3.90-119"][id]))

    proba_default_C = calculate_proba_default(current, defaulters)
    proba_default_0 = calculate_proba_default(delinquency0, defaulters)
    proba_default_1 = calculate_proba_default(delinquency1, defaulters)
    proba_default_2 = calculate_proba_default(delinquency2, defaulters)
    proba_default_3 = calculate_proba_default(delinquency3, defaulters)

    lst_vintage.append("Total")
    lst_applications.append(df[id].nunique())
    lst_defaulters.append(len(defaulters))
    lst_proba_default_C.append(proba_default_C)
    lst_proba_default_0.append(proba_default_0)
    lst_proba_default_1.append(proba_default_1)
    lst_proba_default_2.append(proba_default_2)
    lst_proba_default_3.append(proba_default_3)

    #Final DF
    df_default_proba = pd.DataFrame({
    'vintage': lst_vintage,
    'applications': lst_applications,
    'defaulters': lst_defaulters,
    '00.Current': lst_proba_default_C,
    '0.1-29': lst_proba_default_0,
    '1.30-59': lst_proba_default_1,
    '2.60-89': lst_proba_default_2,
    '3.90-119': lst_proba_default_3,
    })

    return df_default_proba

def run_pipeline(
    file_path:str|None=None,
    verbose:bool=False
):
    
    df_loans, df_payments, df_amt, df_snapshot_m = read_files(file_path)
    df_payments,df_amt = transform_tables(df_payments, df_loans,df_amt)
    df_result, df_result_matrix = get_reporting_tables(
        df_amt,
        df_payments
    )
    df_default_proba = get_default_proba(df_=df_snapshot_m, 
                                         id="application_id",
                                         status="bucket",
                                         vintage="vintage", 
                                         t="t"
                                         )
    if verbose:
        print(df_result.info())
        print(df_result_matrix.info())

    if file_path:
        df_result.to_csv(
            f"{file_path}monthly_bucket_recoveries.csv",
            index=False
        )
        df_default_proba.to_csv(
            f"{file_path}default_proba.csv",
            index=False
        )

        # df_result_matrix.to_csv(
        #     f"{file_path}transition_matrix_total_amount.csv",
        #     index=True
        # )

    print("- Expected recovery ha terminado")
