import re
import numpy as np
import pandas as pd
import unicodedata
from dateutil.parser import parse
from google.gmail import Email
from google.spread import Spread
from src.core.utils import get_data
from src.core.constants import data_sources_google_daily_report


def format_column_name(df: pd.DataFrame) -> pd.DataFrame:
    """
    Formatea los nombres de las columnas en un DataFrame de pandas.

    Esta función toma un DataFrame y aplica una serie de transformaciones a los nombres
    de las columnas para estandarizarlos. Los nombres se convierten a minúsculas, se
    eliminan espacios y caracteres especiales, y se reemplazan ciertos caracteres
    por otros para mejorar la consistencia en los nombres de las columnas.

    Args:
        df (pandas.DataFrame): El DataFrame al que se le desea formatear los nombres de las columnas.

    Returns:
        pandas.DataFrame: Un nuevo DataFrame con los nombres de las columnas formateados.

    Example:
        input_df = ...  # Tu DataFrame original
        formatted_df = format_column_name(input_df)
    """
    # Aplicar las transformaciones a los nombres de las columnas
    df_aux = df.rename(
        columns=lambda x: str(x).lower()
        .strip()
        .replace(" ", "_")
        .replace("\n", "_")
        .replace("(", "")
        .replace(")", "")
        .replace("%", "pct")
    )
    return df_aux

def format_string_columns(df: pd.DataFrame) -> pd.DataFrame:
    str_capitalize = ["estado_final", "causal_rechazo/aceptación"]

    str_columns = [
        (str_capitalize, lambda x: re.sub(" +", " ", x.strip().capitalize()) if pd.notna(x) else x)
    ]

    for columns, transform_func in str_columns:
        for var in columns:
            df[var] = df[var].apply(transform_func)
            df[var] = df[var].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')

    return df

def filter_data(df: pd.DataFrame) -> pd.DataFrame:
    id_cliente_notna = df.id.notna()
    estado_final_values = ["Moto entregada", "Aprobado", "Rechazado", "Proceso incompleto"]

    df = df[id_cliente_notna & df.estado_final.isin(estado_final_values)]

    return df

def transform_date_cols(df: pd.DataFrame, date_columns: list) -> pd.DataFrame:
    df_aux = df.copy()
    for var in date_columns:
        df_aux[var] = df_aux[var].apply(lambda x: parse(x, dayfirst=False, fuzzy=True) if (isinstance(x, str) and x.strip()) else pd.NaT)
        df_aux[var] = pd.to_datetime(df_aux[var]).dt.strftime('%Y-%m-%d')
    return df_aux

def transform_numeric_cols(df: pd.DataFrame, numeric_columns: list) -> pd.DataFrame:
    df_aux = df.copy()
    df_aux[numeric_columns] = df_aux[numeric_columns].apply(pd.to_numeric, errors="coerce")
    return df_aux

def transform_str_cols(df: pd.DataFrame, str_columns: list) -> pd.DataFrame:
    df_aux = df.copy()
    df_aux[str_columns] = df_aux[str_columns].fillna("")
    df_aux[str_columns] = df_aux[str_columns].replace('#N/A.*|<Na>.*', '', regex=True)
    return df_aux

def transform(
    df: pd.DataFrame,
) -> pd.DataFrame:
    df_aux = df.copy()
    #df_aux["tienda"] = df_aux["tienda"].str.split("-").str[0].fillna("")
    
    return df_aux

def get_base_final(
        db: dict, 
        source_version: int,
        vintage, 
        source) -> pd.DataFrame:
    df = get_data(source_version, db,vintage,source)
    df = format_column_name(df)
    df = df[db.get("columns", "")]
    df = df.rename(columns=db.get("rename_columns", ""), inplace=False)
    df['rfc'] = df['rfc'].str.lower()
    df = df.dropna(subset="id", axis=0, how="any")
    df = format_string_columns(df)
    df = transform_date_cols(df, db.get("date_columns", ""))
    df = filter_data(df)
    df = transform(df)
    df = transform_numeric_cols(df, db.get("numeric_columns", ""))
    df = df[db.get("final_columns", "")]
    df = df.replace({np.nan: None})
    df = transform_str_cols(df, db.get("str_columns", ""))
    df = df.astype(db.get("data_types", ""))
    
    return df

def get_buro_validaciones(        
        db: dict, 
        source_version: int,
        vintage, 
        source) -> pd.DataFrame:
    df = get_data(source_version, db,vintage,source)
    df = format_column_name(df)
    df = df[db.get("columns", "")]
    df = df.rename(columns=db.get("rename_columns", ""), inplace=False)
    df['rfc'] = df['rfc'].str.lower()
    df = df.dropna(subset="rfc", axis=0, how="any")
    df = transform_date_cols(df, db.get("date_columns", ""))
    df = df[db.get("final_columns", "")]
    df = df.replace({np.nan: None})
    df = df.astype(db.get("data_types", ""))
    
    return df

def get_comprobante_nomina(
        db: dict, 
        source_version: int,
        vintage, 
        source) -> pd.DataFrame:
    df = get_data(source_version, db,vintage,source)
    df = format_column_name(df)
    df = df[db.get("columns", "")]
    df = df.rename(columns=db.get("rename_columns", ""), inplace=False)
    df['rfc'] = df['rfc'].str.lower()
    df = df.dropna(subset="rfc", axis=0, how="any")
    df = transform_date_cols(df, db.get("date_columns", ""))
    df = df[db.get("final_columns", "")]
    df = df.replace({np.nan: None})
    df = df.astype(db.get("data_types", ""))
    
    return df

def get_estados_clientes(
        db: dict, 
        source_version: int,
        vintage, 
        source) -> pd.DataFrame:
    df = get_data(source_version, db,vintage,source)
    df = format_column_name(df)
    df = df[db.get("columns", "")]
    df = df.rename(columns=db.get("rename_columns", ""), inplace=False)
    df['rfc'] = df['rfc'].str.lower()
    df = df.dropna(subset="rfc", axis=0, how="any")
    df = df[db.get("final_columns", "")]
    df = df.replace({np.nan: None})
    df = df.astype(db.get("data_types", ""))
    
    return df


def get_form_responses(
        db: dict, 
        source_version: int,
        vintage, 
        source) -> pd.DataFrame:
    df = get_data(source_version, db,vintage,source)
    df = format_column_name(df)
    df = df[db.get("columns", "")]
    df = df.rename(columns=db.get("rename_columns", ""), inplace=False)
    df['rfc'] = df['rfc'].str.lower()
    df = df.dropna(subset="rfc", axis=0, how="any")
    df = transform_date_cols(df, db.get("date_columns", ""))
    df = df[db.get("final_columns", "")]
    df = df.replace({np.nan: None})
    df = df.astype(db.get("data_types", ""))
    
    return df

def run_pipeline(vintage,dir_path,source):

    # segunda fuente
    google_sheet_id2 = data_sources_google_daily_report["segunda_fuente"]["google_sheet_id"]
    dict_segunda_fuente = data_sources_google_daily_report["segunda_fuente"]["databases"]
    base_final_db2 = dict_segunda_fuente["base_final"]
    validaciones_db2 = dict_segunda_fuente["validaciones"]
    buro_db2 = dict_segunda_fuente["buro"]
    nomina_db2 = dict_segunda_fuente["mesa_control_comprobante_nomina"]
    estados_db2 = dict_segunda_fuente["estados_clientes"]
    form_responses_db2 = dict_segunda_fuente["form_responses_2"]

    base_final_df = get_base_final(base_final_db2, 2, vintage, source).sort_values('id', ascending=False).drop_duplicates(subset='rfc', keep='first')
    validaciones_df = get_buro_validaciones(validaciones_db2, 2, vintage, source).sort_values("validation_date", ascending=False).drop_duplicates(subset=["rfc"])
    buro_df = get_buro_validaciones(buro_db2, 2, vintage, source).sort_values("buro_date", ascending=False).drop_duplicates(subset=["rfc"])
    nomina_df = get_comprobante_nomina(nomina_db2, 2, vintage, source).sort_values("fecha_causa_estado_proceso_incompleto", ascending=False).drop_duplicates(subset=["rfc"], keep='first')
    estados_df = get_estados_clientes(estados_db2, 2, vintage, source).drop_duplicates(subset=["rfc"], keep='last')
    form_responses_df = get_form_responses(form_responses_db2, 2, vintage, source).sort_values("fecha_accion_realizada", ascending=False).drop_duplicates(subset=["rfc"], keep='first')


    df = base_final_df.merge(validaciones_df, on='rfc', how='left')
    df = df.merge(buro_df, on='rfc', how='left')
    df = df.merge(nomina_df, on='rfc', how='left')
    df = df.merge(estados_df, on='rfc', how='left')
    df = df.merge(form_responses_df, on='rfc', how='left')

    # Procesar los datos y crear las nuevas columnas

    df["causa_estado_proceso_incompleto"] = df.apply(lambda row: row["causa_estado_proceso_incompleto"] if row["estado_final"] == "Proceso incompleto" else None, axis=1)

    def encontrar_causa_rechazo(row):
        causas_rechazo = []
        for col in ['validacion_de_documentos', 'validacion_de_referencias', 'estudio_de_credito', 'resultado_pld']:
            if row[col] in ['Rechazado', 'Devuelto', 'Proceso incompleto']:
                causas_rechazo.append(col)
        return causas_rechazo if causas_rechazo else None

    # Crear la columna "causa_rechazo" aplicando la función encontrar_causa_rechazo en cada fila del DataFrame
    df['causa_estado_rechazo'] = df.apply(encontrar_causa_rechazo, axis=1)
    df['causa_estado_rechazo'] = df.apply(lambda row: row["causa_estado_rechazo"] if row["estado_final"] == "Rechazado" else None, axis=1)

    df["estado_documentos"] = df["timestamp_form_2_documentos"].apply(lambda x: "no ha cargado documentos" if pd.isnull(x) else "documentos cargados")
    df["estado_validacion"] = df["resultado_final_de_verificacion"].apply(lambda x: "Validado mesa de control" if x == "Validado" else ("En trabajo mesa de control" if x == "En trabajo" else "Rechazado mesa de control"))
    df["estado_consulta_buro"] = df["estatus_consulta_y_validación"].apply(lambda x: "Consultado Buro" if x == "Consultado" else "No consultado Buro")
    df["estado_contrato_anexo"] = df["timestamp_form_4"].apply(lambda x: "Contratos y anexos" if pd.notnull(x) else None)
    df["estado_moto_entregada"] = df["timestamp_moto_entregada_/_contrato_firmado"].apply(lambda x: "Moto entregada" if pd.notnull(x) else None)
    df["dias_documentos"] = (df["timestamp_form_2_documentos"] - df["timestamp_form_1_solicitud"]).abs().dt.days
    df["dias_validacion"] = (df["timestamp_form_2_documentos"] - df["validation_date"]).abs().dt.days
    df["dias_consulta_buro"] = (df["validation_date"] - df["buro_date"]).abs().dt.days
    df["dias_contrato_anexo"] = (df["timestamp_form_4"] - df["validation_date"]).abs().dt.days
    df["dias_moto_entregada"] = (df["timestamp_moto_entregada_/_contrato_firmado"] - df["timestamp_correo_aprobado"]).abs().dt.days

    # Columnas de relación estado-días en orden ascendente
    relacion_columnas = [
        ("estado_documentos", "dias_documentos"),
        ("estado_validacion", "dias_validacion"),
        ("estado_consulta_buro", "dias_consulta_buro"),
        ("estado_contrato_anexo", "dias_contrato_anexo"),
        ("estado_moto_entregada", "dias_moto_entregada")
    ]
    # Encontrar la última relación donde los días son diferentes a nulo
    detalle_estado = None
    detalle_dias_estado = None
    for estado, dias in relacion_columnas:
        if pd.notnull(df[dias]).any():
            df['detalle_estado'] = df[estado]
            df['detalle_dias_estado'] = df[dias]


            
    df = df[
        [
        'id', 
        'nombre', 
        'rfc', 
        "timestamp_moto_entregada_/_contrato_firmado",
        'estado_del_contrato',
        'estado_final', 
        'causa_estado_proceso_incompleto',
        'fecha_causa_estado_proceso_incompleto',
        'causa_estado_rechazo',
        'tienda',
        'fecha_aplicación',
        'estado_documentos',
        'estado_validacion', 
        'estado_consulta_buro', 
        'estado_contrato_anexo',
        'estado_moto_entregada', 
        'dias_documentos', 
        'dias_validacion',
        'dias_consulta_buro', 
        'dias_contrato_anexo', 
        'dias_moto_entregada',
        'detalle_estado',
        'detalle_dias_estado',
        'resultado_final_de_verificacion',
        'validation_date',
        'accion_realizada',
        'fecha_accion_realizada'
        ]
    ].rename(
        columns={
            "timestamp_moto_entregada_/_contrato_firmado": "booking_date"
        }
    )

    df = df.iloc[23:].reset_index().drop(columns='index')

    df['estado_del_contrato'] = df['estado_del_contrato'].str.replace("#N/A.*", "", regex=True).str.strip()

    df.to_excel("{}reporte_diario_waldos_motos.xlsx".format(dir_path))

    # Crear una instancia de la clase Email
    email = Email()

    # Configurar los detalles del correo electrónico
    email.to = [
        'leidy.velandia@tangelolatam.com',
        'alex.yi@tangelolatam.com',
        'karina.hernandez@tangelolatam.com',
        'carolina.gonzalez@tangelolatam.com',
        'cristobald@grupovizion.com.mx',
        'arnoldo.gabaldon@tangelolatam.com',
#        'valeria.castano@tangelolatam.com',
        'arturo.lartundo@tangelolatam.com',
        'alejandro.pacheco@tangelolatam.com',
        'tavarezr@grupovizion.com.mx',
        'osvaldo.padilla@tangelolatam.com',
        'zamoraot@grupovizion.com.mx',
        'calderonr@grupovizion.com.mx',
        'broncaa@mdqr.com.mx',
#        'daniel.perdomo@tangelolatam.com'
    ]
    email.subject = 'Reporte diario Waldos Motos'
    email.body = 'Se envia reporte diario de waldos motos. Quedo atenta a sus comentarios.'

    # Configurar adjuntos
    email.attachments = ['{}reporte_diario_waldos_motos.xlsx'.format(dir_path)]

    # Configurar el cuerpo del mensaje como HTML
    email.as_html = True

    # Enviar el correo electrónico
    email.send_message()

    print("- Daily Waldos Report ha terminado")