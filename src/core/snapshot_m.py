import pandas as pd
import numpy as np
from datetime import timedelta
from operator import attrgetter
from src.core.calculator.utils import calculate_bucket

def read_files(
    dir_core:str="./data/core/outputs/"
):
    
    df_loans = pd.read_csv(
        f"{dir_core}loans.csv"
    )

    df_payments = pd.read_csv(
         f"{dir_core}payments.csv"
    ).drop_duplicates(
        subset=["application_id", "payment_number"]
    )

    df_amortization = pd.read_csv(
        f"{dir_core}amortizacion_waldos_motos.csv"
    )

    return df_loans, df_payments, df_amortization

def transform_payments_data(
        df_payments
):
    
    vars_to_cumm = [
        "capital",
        "payment_amount",
        "interest",
        "iva",
    ]

    df_payments["created_date"] = pd.to_datetime(df_payments["created_date"])
    df_payments = df_payments.sort_values(
        by=[
            "application_id",
            "created_date"
        ]
    )

    df_payments["payment_amount_cumm"] = df_payments.groupby("application_id").payment_amount.transform("cumsum")

    for val_to_cumm in vars_to_cumm:
        df_payments[f"{val_to_cumm}_paid"] = df_payments.groupby('application_id')[f"{val_to_cumm}"].cumsum()

    return df_payments

def transform_amortization_data(
    vintage,
    df_amortization,
    df_payments,
    df_loans
    
) -> pd.DataFrame:
    
    limit_date = pd.to_datetime(vintage, format="%Y%m%d").strftime("%Y-%m-%d")
    cols_to_date = [
        "completed_date",
        "payment_date",
    ]

    df_amortization_t = pd.merge_asof(
        df_amortization.drop(columns="completed_date").sort_values(by='payment_amount_cumm').astype(
            {
                "application_id":int,
                "payment_amount_cumm":float
            }
        ),
        df_payments[
            [
                'application_id',
                'payment_amount_cumm',
                'created_date',
            ]
        ].rename(
            columns={
                'created_date':'completed_date'
            }
        ).sort_values(by='payment_amount_cumm').astype(
            {
                "application_id":int,
                "payment_amount_cumm":float
            }
        ),
        by='application_id',
        on='payment_amount_cumm',
        direction='forward',
    )

    df_amortization_t = pd.merge(
        df_amortization_t,
        df_loans[
            [
                "application_id",
                "booking_date",
            ]
        ],
        how='left',
        on='application_id'
    )

    for col in cols_to_date:
        df_amortization_t[col] = pd.to_datetime(df_amortization_t[col], errors='coerce') 


    df_amortization_t['is_late'] = np.where(
        df_amortization_t.payment_date <= limit_date,
        np.where(
            df_amortization_t.completed_date.isna(),
            True,
            False
        ),
        np.nan
    )

    df_amortization_t['is_prepayment'] = df_amortization_t['completed_date'] < df_amortization_t['payment_date']  

    df_amortization_t['payment_date_aux'] =  pd.to_datetime(
        np.where(
            df_amortization_t.is_late == 1,
            df_amortization_t['payment_date'],
            pd.NaT
        )
    )

    df_amortization_t['late_since'] = df_amortization_t.groupby('application_id').payment_date_aux.transform(min)
    df_amortization_t.loc[df_amortization_t['is_late'] == 0, 'late_since'] = pd.NaT

    df_amortization_t = df_amortization_t.drop(
        columns='payment_date_aux'
    )

    df_amortization_t['max_date'] = np.where(
        df_amortization_t.completed_date.isna(),
        df_amortization_t.payment_date,
        df_amortization_t.completed_date,
    )

    df_amortization_t['date_merge'] = np.where(
        df_amortization_t.is_prepayment,
        df_amortization_t.completed_date,
        df_amortization_t.payment_date,
    )

    df_amortization_t['dpd'] = (df_amortization_t.payment_date - df_amortization_t.late_since).dt.days

    df_amortization_t['over30'] = np.where(
        df_amortization_t.completed_date.combine_first(df_amortization_t.payment_date) < limit_date,
        df_amortization_t.dpd > 30,
        np.nan
    )

    return df_amortization_t

def get_snapshot_m(
    vintage:str,
    df_amortization_t:pd.DataFrame,
    df_payments:pd.DataFrame,
    df_loans:pd.DataFrame,
    init_date_month:str="2022-04-30"
) -> pd.DataFrame:
    
    limit_date = pd.to_datetime(vintage, format="%Y%m%d").strftime("%Y-%m-%d")
    first_date_month = pd.to_datetime(limit_date, format="%Y-%m-%d").replace(day=1)
    last_date_month = (first_date_month - timedelta(days=1)).strftime("%Y-%m-%d")

    lst_snapshots_m = []
    for vintage_date in list(pd.date_range(start=init_date_month, end=last_date_month, freq='M')) + [limit_date]:
        df_aux = df_amortization_t[
            df_amortization_t.date_merge <= vintage_date
        ].drop(
            columns=[
                'dpd',
                'is_late',
                'max_date',
                'late_since',
                "over30",
                "payment_amount_cumm",
                "payment_amount",
                "principal",
                "interest",
                "iva",
            ]
        )
        
        df_payments_aux = df_payments[
            df_payments.created_date <= vintage_date
        ][
            [
                "application_id",
                "payment_number",
                "capital_paid",
                "payment_amount_paid",
                "interest_paid",
                "iva_paid",
            ]
        ].copy()
        
        df_payments_aux = df_payments_aux[
            df_payments_aux.groupby("application_id").payment_number.transform(max) == df_payments_aux.payment_number
        ].drop(
            columns="payment_number"
        )
        
        df_aux = pd.merge(
            df_aux,
            df_payments_aux,
            how="left",
            on="application_id"
        )

        # Se revisa que el completed date sea previo al vintage date 
        df_aux['completed_date_aux'] = pd.to_datetime(
            np.where(
                df_aux.completed_date <= vintage_date,
                df_aux.completed_date,
                pd.NaT
            )
        )

        df_aux['is_late'] = df_aux['completed_date_aux'].isna()

        df_aux['payment_date_aux'] =  pd.to_datetime(
            np.where(
                df_aux.is_late == 1,
                df_aux['payment_date'],
                pd.NaT
            )
        )

        df_aux['late_since'] = df_aux.groupby('application_id').payment_date_aux.transform(min) 

        df_aux['dpd'] = np.maximum((df_aux.completed_date_aux.fillna(vintage_date) - df_aux.late_since).dt.days,0)
        df_aux['max_dpd'] = np.maximum((df_aux.completed_date_aux.fillna(vintage_date) - df_aux.payment_date).dt.days,0)
        df_aux['ever_dpd'] = df_aux.groupby('application_id')['max_dpd'].transform(max)
        df_aux['last_payment'] = df_aux.groupby('application_id')['completed_date_aux'].transform(max)


        df_aux = df_aux.drop(
            columns = [
                "payment_date_aux",
                "completed_date_aux",
                "date_merge"
            ]
        )

        df_aux = df_aux[
            df_aux.groupby("application_id").payment_number.transform(max) == df_aux.payment_number
        ].drop(
            columns = [
                "is_prepayment",
                "completed_date",
                "payment_date"
            ]
        )
        
        df_aux['month_end'] = pd.to_datetime(vintage_date)

        lst_snapshots_m.append(df_aux)
        
    df_snapshot_m = pd.concat(lst_snapshots_m)

    df_snapshot_m = pd.merge(
        df_snapshot_m,
        df_loans[
            [
                "application_id",
                "loan_amount",
            ]
        ],
        how='left',
        on='application_id'
    )

    df_snapshot_m = df_snapshot_m.rename(
        columns={
            "capital_paid":"principal_paid"
        }
    )

    df_snapshot_m["paydown"] = df_snapshot_m.principal_paid / df_snapshot_m.loan_amount

    for dpd in [30,60,90,120]:
        df_snapshot_m[f'over{dpd}'] = (df_snapshot_m.dpd > dpd).astype(int)
        
    for dpd in [30,60,90,120]:
        df_snapshot_m[f'ever{dpd}'] = (df_snapshot_m.ever_dpd > dpd).astype(int)

    df_snapshot_m["booking_date"] = pd.to_datetime(df_snapshot_m.booking_date)
    df_snapshot_m['bucket'] = calculate_bucket(df_snapshot_m.dpd)    
    df_snapshot_m["vintage"] = df_snapshot_m.booking_date.dt.strftime("%Y%m")
    vintage2 = pd.to_datetime(df_snapshot_m["vintage"],  format='%Y%m')
    df_snapshot_m["t"] = (df_snapshot_m["month_end"].dt.to_period('M') - vintage2.dt.to_period('M')).apply(attrgetter('n'))
    
    df_snapshot_m = df_snapshot_m[
        df_snapshot_m.t >= 0 
    ]


    return df_snapshot_m

def get_transition_matrix(df_, id_, status, time):
    df=df_.copy()
    df.sort_values(by=[id_, time])
    status2="next_"+status
    df[status2] = df.groupby(id_)[status].shift(-1)
    df.dropna(subset=[status2, status], inplace=True)
    transition_matrix = pd.crosstab([df[status]], df[status2]
        ,normalize=True
        )
    transition_matrix = transition_matrix.round(3)
    return transition_matrix, df

def run_pipeline(
    vintage:int,
    file_path:str|None=None,
    verbose:bool=False
):
    df_loans, df_payments, df_amortization = read_files()
    df_payments = transform_payments_data(df_payments)
    df_amortization_t = transform_amortization_data(
        vintage,
        df_amortization,
        df_payments,
        df_loans
    )
    
    df_snapshot_m = get_snapshot_m(
        vintage,
        df_amortization_t,
        df_payments,
        df_loans
    )
    _, transition_matrix_m = get_transition_matrix(df_=df_snapshot_m, 
                                        id_="application_id", 
                                        status="bucket",
                                        time="month_end")
    transition_matrix_m["balance"] = transition_matrix_m["loan_amount"] - transition_matrix_m["principal_paid"]
    transition_matrix_m = transition_matrix_m.groupby(['vintage','bucket','next_bucket']).agg({
        'application_id':'count',
        'loan_amount':'sum',
        'balance':'sum'
        })
    
    if verbose:
        print(df_snapshot_m.info())

    if file_path:
        df_snapshot_m.to_csv(
            f"{file_path}snapshot_m.csv",
            index=False
        )
        
        transition_matrix_m.to_csv(
            f"{file_path}transition_matrix_m.csv",
        )
        
    print("- Snapshot ha terminado")
