import re
import numpy as np
import pandas as pd
import unicodedata
from dateutil.parser import parse
from src.core.utils import get_data
from src.core.constants import data_sources_google_apps
import datetime

def format_column_name(df: pd.DataFrame) -> pd.DataFrame:
    """
    Formatea los nombres de las columnas en un DataFrame de pandas.

    Esta función toma un DataFrame y aplica una serie de transformaciones a los nombres
    de las columnas para estandarizarlos. Los nombres se convierten a minúsculas, se
    eliminan espacios y caracteres especiales, y se reemplazan ciertos caracteres
    por otros para mejorar la consistencia en los nombres de las columnas.

    Args:
        df (pandas.DataFrame): El DataFrame al que se le desea formatear los nombres de las columnas.

    Returns:
        pandas.DataFrame: Un nuevo DataFrame con los nombres de las columnas formateados.

    Example:
        input_df = ...  # Tu DataFrame original
        formatted_df = format_column_name(input_df)
    """
    # Aplicar las transformaciones a los nombres de las columnas
    df_aux = df.rename(
        columns=lambda x: str(x).lower()
        .strip()
        .replace(" ", "_")
        .replace("\n", "_")
        .replace("(", "")
        .replace(")", "")
        .replace("%", "pct")
    )
    return df_aux


def format_string_columns(df: pd.DataFrame) -> pd.DataFrame:
    """
    Formatea las columnas de tipo string en un DataFrame de pandas.

    Esta función toma un DataFrame y aplica transformaciones específicas a las columnas
    de tipo string. Puede capitalizar y titularizar ciertas columnas, además de eliminar
    caracteres especiales. También se realiza un proceso de normalización de caracteres
    Unicode para manejar caracteres acentuados u especiales.

    Args:
        df (pandas.DataFrame): El DataFrame al que se le desea formatear las columnas de tipo string.

    Returns:
        pandas.DataFrame: Un nuevo DataFrame con las columnas de tipo string formateadas.

    Example:
        input_df = ...  # Tu DataFrame original
        formatted_df = format_string_columns(input_df)
    """
    str_capitalize = [
        "estado_final",
        "causal_rechazo/aceptación",
        "moto_entregada_/_contrato_firmado",
    ]
    str_title = ["requested_product", "approved_product"]

    str_columns = [
        (
            str_capitalize,
            lambda x: re.sub(" +", " ", x.strip().capitalize()) if pd.notna(x) else x,
        ),
        (
            str_title,
            lambda x: re.sub(" +", " ", x.strip().title()) if pd.notna(x) else x,
        ),
    ]
    df["causal_rechazo/aceptación"] = df["causal_rechazo/aceptación"].replace(False,'repetido')
    for columns, transform_func in str_columns:
        for var in columns:   
            if not df[var].isna().all():
                df[var] = df[var].apply(transform_func)
            df[var] = (
                df[var]
                .str.normalize("NFKD")
                .str.encode("ascii", errors="ignore")
                .str.decode("utf-8")
            )

    return df


def filter_data(df: pd.DataFrame) -> pd.DataFrame:
    """
    Filtra un DataFrame de pandas según ciertos criterios.

    Esta función toma un DataFrame y aplica filtros para mantener únicamente las filas
    que cumplan con los criterios establecidos. Filtra las filas en las que la columna 'id'
    no sea nula (notna) y la columna 'estado_final' tenga valores específicos.

    Args:
        df (pandas.DataFrame): El DataFrame que se desea filtrar.

    Returns:
        pandas.DataFrame: Un nuevo DataFrame con las filas filtradas.

    Example:
        input_df = ...  # Tu DataFrame original
        filtered_df = filter_data(input_df)
    """
    id_cliente_notna = df.id.notna()
    estado_final_values = [
        "Moto entregada",
        "Aprobado",
        "Rechazado",
        "Proceso incompleto",
        "Cancelado"
    ]

    # Aplicar los filtros a las filas del DataFrame
    df = df[id_cliente_notna & df.estado_final.isin(estado_final_values)]

    return df


def transform_date_cols(
    df: pd.DataFrame, date_columns: list, fuente: int = 2
) -> pd.DataFrame:
    """
    Transforma columnas de fechas en un DataFrame de pandas.

    Esta función toma un DataFrame y realiza transformaciones en las columnas de fechas
    especificadas en la lista 'date_columns'. Dependiendo de la fuente de los datos,
    se aplicarán diferentes configuraciones para el análisis de las fechas.

    Args:
        df (pandas.DataFrame): El DataFrame que contiene las columnas de fechas a transformar.
        date_columns (list): Lista de nombres de columnas que contienen fechas.
        fuente (int): Indicador de la fuente de los datos, 1 o 2.

    Returns:
        pandas.DataFrame: Un nuevo DataFrame con las columnas de fechas transformadas.

    Example:
        input_df = ...  # Tu DataFrame original
        date_cols = ['fecha_inicio', 'fecha_fin']  # Nombres de las columnas de fechas
        source = 1  # Indicador de la fuente de los datos
        transformed_df = transform_date_cols(input_df, date_cols, source)
    """
    df_aux = df.copy()
    if fuente == 1:
        for var in date_columns:
            df_aux[var] = df_aux[var].apply(
                lambda x: parse(x, dayfirst=True, fuzzy=True)
                if (isinstance(x, str) and x.strip())
                else pd.NaT
            )
            df_aux[var] = pd.to_datetime(
                pd.to_datetime(df_aux[var]).dt.strftime("%Y-%m-%d %H:%M:%S")
            )
    elif fuente == 2:
        for var in date_columns:
            df_aux[var] = df_aux[var].apply(
                lambda x: parse(x, dayfirst=False, fuzzy=True)
                if (isinstance(x, str) and x.strip())
                else pd.NaT
            )
            df_aux[var] = pd.to_datetime(
                pd.to_datetime(df_aux[var]).dt.strftime("%Y-%m-%d %H:%M:%S")
            )
    return df_aux


def transform_numeric_cols(df: pd.DataFrame, numeric_columns: list) -> pd.DataFrame:
    """
    Transforma columnas numéricas en un DataFrame de pandas.

    Esta función toma un DataFrame y realiza la transformación de las columnas numéricas
    especificadas en la lista 'numeric_columns'. Las columnas se convierten al tipo numérico,
    y los valores no válidos se tratan como NaN (Not a Number).

    Args:
        df (pandas.DataFrame): El DataFrame que contiene las columnas numéricas a transformar.
        numeric_columns (list): Lista de nombres de columnas que contienen datos numéricos.

    Returns:
        pandas.DataFrame: Un nuevo DataFrame con las columnas numéricas transformadas.

    Example:
        input_df = ...  # Tu DataFrame original
        num_cols = ['edad', 'ingresos']  # Nombres de las columnas numéricas
        transformed_df = transform_numeric_cols(input_df, num_cols)
    """
    df_aux = df.copy()
    df_aux[numeric_columns] = df_aux[numeric_columns].apply(
        pd.to_numeric, errors="coerce"
    )
    return df_aux


def transform_str_cols(df: pd.DataFrame, str_columns: list) -> pd.DataFrame:
    """
    Transforma columnas de tipo string en un DataFrame de pandas.

    Esta función toma un DataFrame y realiza la transformación de las columnas de tipo string
    especificadas en la lista 'str_columns'. Se llenan los valores faltantes con cadena vacía,
    y se eliminan los patrones '#N/A.*' y '<Na>.*' de los valores en las columnas.

    Args:
        df (pandas.DataFrame): El DataFrame que contiene las columnas de tipo string a transformar.
        str_columns (list): Lista de nombres de columnas que contienen datos de tipo string.

    Returns:
        pandas.DataFrame: Un nuevo DataFrame con las columnas de tipo string transformadas.

    Example:
        input_df = ...  # Tu DataFrame original
        str_cols = ['descripcion', 'observaciones']  # Nombres de las columnas de tipo string
        transformed_df = transform_str_cols(input_df, str_cols)
    """
    df_aux = df.copy()
    df_aux[str_columns] = df_aux[str_columns].fillna("")
    df_aux[str_columns] = df_aux[str_columns].replace("#N/A.*|<Na>.*", "", regex=True)
    return df_aux


def transform(df: pd.DataFrame, fuente: int) -> pd.DataFrame:
    df_aux = df.copy()
    # store_id
    df_aux["store_id"] = df_aux["tienda"].str.split("-").str[0].fillna("")

    # status_id
    condiciones = (
        lambda x: 1
        if x == "Moto entregada"
        else 2
        if x == "Aprobado"
        else 3
        if x == "Rechazado"
        else 4
        if x == "Proceso incompleto"
        else 5
        if x == "Cancelado"
        else None
    )
    df_aux["status_id"] = df_aux["estado_final"].apply(condiciones)

    # booked
    condiciones = lambda x: 1 if x == "Moto entregada" else 0
    df_aux["booked"] = df_aux["estado_final"].apply(condiciones)

    # completed
    condiciones = lambda x: 1 if x != "Proceso incompleto" else 0
    df_aux["completed"] = df_aux["estado_final"].apply(condiciones)

    # risk_decision
    df_aux["estado_final"] = df_aux["estado_final"].fillna("")
    condiciones = (
        lambda x: 1
        if x == "Aprobado" or x == "Moto entregada"
        else 0
        if x == "Rechazado"
        else -1
    )
    df_aux["risk_decision"] = df_aux.estado_final.apply(condiciones)

    # rejection_cause_risk
    df_aux["causal_rechazo/aceptación"] = df_aux["causal_rechazo/aceptación"].fillna("")
    condiciones = (
        lambda x: x
        if x == "Los score no superan el umbral para aprobación"
        or x == "Su capacidad de pago no es suficiente"
        else None
    )
    df_aux["rejection_cause_risk"] = df_aux["causal_rechazo/aceptación"].apply(
        condiciones
    )

    # rejection_cause_kyc
    condiciones = (
        lambda x: x
        if x == "No se pudo validar el domicilio"
        or x == "No se pudo validar la ocupación"
        else None
    )
    df_aux["rejection_cause_kyc"] = df_aux["causal_rechazo/aceptación"].apply(
        condiciones
    )

    # rejection_cause
    condlist = [
        (~df_aux["rejection_cause_risk"].isna()),
        (~df_aux["rejection_cause_kyc"].isna()),
    ]
    choicelist = ["risk", "kyc"]
    df_aux["rejection_cause"] = np.select(condlist, choicelist, default=None)

    # frequency
    df_aux["frequency"] = 2

    # approved
    condlist = [
        (df_aux["rejection_cause"].isna()) & (df_aux["risk_decision"] == 1),
    ]
    choicelist = [1]
    df_aux["approved"] = np.select(condlist, choicelist, default=0)

    # signed_contract

    condlist = [
        (df_aux["moto_entregada_/_contrato_firmado"] == "Si"),
        (df_aux["moto_entregada_/_contrato_firmado"] == "No"),
    ]
    choicelist = [1, 0]
    df_aux["signed_contract"] = np.select(condlist, choicelist, default=np.nan)

    if fuente == 2:
        # Crear la columna "fill_basic_documentation"
        df_aux["fill_basic_documentation"] = df_aux.apply(
            lambda row: None
            if pd.isnull(row["document_date"])
            else None
            if row["document_date"] < row["request_date"]
            else (row["document_date"] - row["request_date"]).total_seconds(),
            axis=1,
        )

        # Crear la columna "decision_time"
        def calculate_timediff(row):
            if pd.isnull(row["document_date"]):
                return None
            elif pd.isnull(row["approved_email_date"]):
                if pd.isnull(row["rejected_email_date"]):
                    return None
                elif (
                    row["rejected_email_date"] - row["document_date"]
                ).total_seconds() < 0:
                    return None
                else:
                    return (
                        row["rejected_email_date"] - row["document_date"]
                    ).total_seconds()
            else:
                if (
                    row["approved_email_date"] - row["document_date"]
                ).total_seconds() < 0:
                    return None
                else:
                    return (
                        row["approved_email_date"] - row["document_date"]
                    ).total_seconds()

        df_aux["decision_time"] = df.apply(calculate_timediff, axis=1)

        # Crear la columna "credit_formalization_contract_signing"
        df_aux["credit_formalization_contract_signing"] = df_aux.apply(
            lambda row: None
            if pd.isnull(row["signed_contract_date"])
            else None
            if pd.isnull(row["approved_email_date"])
            else None
            if row["signed_contract_date"] < row["approved_email_date"]
            else (
                row["signed_contract_date"] - row["approved_email_date"]
            ).total_seconds(),
            axis=1,
        )

    return df_aux


def get_base_final(
        db: dict, 
        source_version: int,
        vintage, 
        source) -> pd.DataFrame:
    df = get_data(source_version, db,vintage,source)
    df = format_column_name(df)
    df = df[db.get("columns", "")]
    df = df.rename(columns=db.get("rename_columns", ""), inplace=False)
    df = df.dropna(subset="id", axis=0, how="any")
    df = format_string_columns(df)
    df = transform_date_cols(df, db.get("date_columns", ""), source_version)
    df = filter_data(df)
    df = transform(df, source_version)
    df = transform_numeric_cols(df, db.get("numeric_columns", ""))
    df = df[db.get("final_columns", "")]
    df = df.replace({np.nan: None})
    df = transform_str_cols(df, db.get("str_columns", ""))
    df = df.astype(db.get("data_types", ""))
    df = df.drop_duplicates(subset="id", keep="last")

    return df

def get_capacidad_pago(
        db: dict, 
        source_version: int,
        vintage, 
        source) -> pd.DataFrame:
    df = get_data(source_version, db,vintage,source)
    df = format_column_name(df)
    df = df[db.get("columns", "")]
    df = df.rename(columns=db.get("rename_columns", ""), inplace=False)
    df = df.dropna(subset="id", axis=0, how="any")
    df = transform_numeric_cols(df, db.get("numeric_columns", ""))
    df = df.replace({np.nan: None})
    df = df.astype(db.get("data_types", ""))
    df = df.drop_duplicates(subset="id", keep="last")

    return df


def get_comprobante_nomina(  
        db: dict, 
        source_version: int,
        vintage, 
        source) -> pd.DataFrame:
    df = get_data(source_version, db,vintage,source)
    df = format_column_name(df)
    df = df[db.get("columns", "")]
    df = df.rename(columns=db.get("rename_columns", ""), inplace=False)
    df = df.dropna(subset="rfc", axis=0, how="any")
    df = transform_date_cols(df, db.get("date_columns", ""), source_version)
    df = df.replace({np.nan: None})
    df = transform_str_cols(df, db.get("str_columns", ""))
    df = df.astype(db.get("data_types", ""))
    df = df.sort_values(
        by="control_table_payroll_check_date", ascending=False
    ).drop_duplicates(subset="rfc", keep="first")
    df = df[
        df["confirmation_validation_of_documents"].apply(
            lambda value: isinstance(value, str) and "devuelto" in value.lower()
        )
    ]

    return df


def get_seguimiento_contratos(
        db: dict, 
        source_version: int,
        vintage, 
        source) -> pd.DataFrame:
    df = get_data(source_version, db,vintage,source)
    df = format_column_name(df)
    df = df[db.get("columns", "")]
    df = df.rename(columns=db.get("rename_columns", ""), inplace=False)
    df = df.dropna(subset="rfc", axis=0, how="any")
    df = df.replace({np.nan: None})
    df = transform_str_cols(df, db.get("str_columns", ""))
    df = df.astype(db.get("data_types", ""))
    df = df.drop_duplicates(subset="rfc", keep="last")

    return df


def get_form_responses_2(db: dict, 
        source_version: int,
        vintage, 
        source) -> pd.DataFrame:
    df = get_data(source_version, db,vintage,source)
    df = format_column_name(df)
    df = df[db.get("columns", "")]
    df = df.rename(columns=db.get("rename_columns", ""), inplace=False)
    df = df.dropna(subset="rfc", axis=0, how="any")
    df = transform_date_cols(df, db.get("date_columns", ""), source_version)
    df = df.replace({np.nan: None})
    df = transform_str_cols(df, db.get("str_columns", ""))
    df = df.astype(db.get("data_types", ""))
    df = df.sort_values(by="form_responses_2_date", ascending=False).drop_duplicates(
        subset="rfc", keep="first"
    )
    df = df[df["form_responses_2_action_type"] == "Documentación corrección"]

    return df

def get_buro_validaciones(db: dict, 
        source_version: int,
        vintage, 
        source) -> pd.DataFrame:
    df = get_data(source_version, db,vintage,source)
    df = format_column_name(df)
    df = df[db.get("columns", "")]
    df = df.rename(columns=db.get("rename_columns", ""), inplace=False)
    df['rfc'] = df['rfc'].str.lower()
    df = df.dropna(subset="rfc", axis=0, how="any")
    df = transform_date_cols(df, db.get("date_columns", ""), source_version)
    df = df[db.get("final_columns", "")]
    df = df.replace({np.nan: None})
    df = df.astype(db.get("data_types", ""))
    
    return df


def run_pipeline(vintage,dir_path,source):
    # primera fuente
    google_sheet_id1 = data_sources_google_apps["primera_fuente"]["google_sheet_id"]
    dict_primera_fuente = data_sources_google_apps["primera_fuente"]["databases"]

    base_final_db1 = dict_primera_fuente["base_final"]
    capacidad_pago_db1 = dict_primera_fuente["capacidad_pago"]

    # segunda fuente
    google_sheet_id2 = data_sources_google_apps["segunda_fuente"]["google_sheet_id"]
    dict_segunda_fuente = data_sources_google_apps["segunda_fuente"]["databases"]

    base_final_db2 = dict_segunda_fuente["base_final"]
    capacidad_pago_db2 = dict_segunda_fuente["capacidad_pago"]
    comprobante_nomina_db2 = dict_segunda_fuente["mesa_control_comprobante_nomina"]
    seguimiento_contratos_db2 = dict_segunda_fuente["seguimiento_contratos"]
    form_responses_2_db2 = dict_segunda_fuente["form_responses_2"]
    validaciones_db2 = dict_segunda_fuente["validaciones"]
    buro_db2 = dict_segunda_fuente["buro"]


    base_final_df1 = get_base_final(base_final_db1, 1, vintage, source)
    base_final_df2 = get_base_final(base_final_db2, 2, vintage, source)
    base_final_df = pd.concat([base_final_df1, base_final_df2])

    capacidad_pago_df1 = get_capacidad_pago(capacidad_pago_db1, 1, vintage, source)
    capacidad_pago_df2 = get_capacidad_pago(capacidad_pago_db2, 2, vintage, source)
    capacidad_pago_df = pd.concat([capacidad_pago_df1, capacidad_pago_df2])

    comprobante_nomina_df = get_comprobante_nomina(
        comprobante_nomina_db2, 2, vintage, source
    )

    seguimiento_contratos_df = get_seguimiento_contratos(
        seguimiento_contratos_db2, 2, vintage, source
    )

    form_responses_2_df = get_form_responses_2(
        form_responses_2_db2, 2, vintage, source
    )

    validaciones_df = get_buro_validaciones(validaciones_db2, 2, vintage, source).sort_values("validation_date", ascending=False).drop_duplicates(subset=["rfc"])
    buro_df = get_buro_validaciones(buro_db2, 2, vintage, source).sort_values("buro_date", ascending=False).drop_duplicates(subset=["rfc"])

    df = base_final_df.merge(capacidad_pago_df, on="id", how="left")
    df = df.drop_duplicates(subset="id", keep="first")

    df = df.merge(comprobante_nomina_df, on="rfc", how="left")

    df = df.merge(seguimiento_contratos_df, on="rfc", how="left")

    df = df.merge(form_responses_2_df, on="rfc", how="left")

    df = df.merge(validaciones_df, on='rfc', how='left')

    df = df.merge(buro_df, on='rfc', how='left')

    # Crear la columna "customer_uploaded_correction"
    df["customer_uploaded_correction"] = df.apply(
        lambda row: 1
        if row["form_responses_2_date"] >= row["control_table_payroll_check_date"]
        else (
            0
            if row["form_responses_2_date"] < row["control_table_payroll_check_date"]
            else None
        ),
        axis=1,
    )

    # Crear la columna "control_table_times"
    df["control_table_times"] = df.apply(
        lambda row: (
            (row["form_responses_2_date"] - row["control_table_payroll_check_date"]).total_seconds()
            if pd.notna(row["form_responses_2_date"]) and pd.notna(row["control_table_payroll_check_date"]) and row["status_id"] == 4
            else 0
        ),
        axis=1,
    )

    df['tiempo_validaciones'] = (df['validation_date'] - df['form_responses_2_date']).abs().dt.total_seconds()/3600
    df['tiempo_pld'] = (df['timestamp_pld'] - df['form_responses_2_date']).abs().dt.total_seconds()/3600
    df['tiempo_buro'] = (df['buro_date'] - df['timestamp_pld']).abs().dt.total_seconds()/3600
    df['tiempo_nomina'] = (df['control_table_payroll_check_date'] - df['timestamp_pld']).abs().dt.total_seconds()/3600
    df['tiempo_total'] = df[['tiempo_pld', 'tiempo_buro', 'tiempo_nomina']].max(axis=1)

    df.to_csv(
        "{0}applications.csv".format(dir_path),
        index=False,
        sep=";",
        date_format="%Y-%m-%d %H:%M:%S",
    )

    print("- Applications ha terminado")