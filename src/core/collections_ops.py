import pandas as pd
import numpy as np

def read_files(
    dir_core:str="./data/core/outputs/"
):
    
    df_loans = pd.read_csv(
        f"{dir_core}loans.csv"
    )

    df_snap = pd.read_csv(
        f"{dir_core}snapshot_m.csv"
    )

    df_amortizacion = pd.read_csv(
        f"{dir_core}amortizacion_waldos_motos.csv"
    )

    return df_loans, df_snap, df_amortizacion

def transform_init_data(
    vintage:str,
    df_amortizacion:pd.DataFrame,
):
    limit_date = pd.to_datetime(vintage, format="%Y%m%d").strftime("%Y-%m-%d")
    df_amortization_1 = df_amortizacion[
        df_amortizacion.payment_date <= limit_date
    ]

    df_amortization_1 = df_amortization_1.groupby(
        "application_id",
        as_index=False
    ).agg(
        payment_amount = pd.NamedAgg(column="payment_amount", aggfunc=min),
        number_of_payments_due = pd.NamedAgg(column="payment_amount", aggfunc="count"),
        payment_amount_due = pd.NamedAgg(column="payment_amount", aggfunc=sum),
        principal_due = pd.NamedAgg(column="principal", aggfunc=sum),
        interest_due = pd.NamedAgg(column="interest", aggfunc=sum),
        iva_due = pd.NamedAgg(column="iva", aggfunc=sum),
    )

    df_amortization_2 = df_amortizacion[
        (df_amortizacion.payment_date >= limit_date)
    ]

    df_amortization_2 = df_amortization_2.groupby(
        "application_id",
        as_index=False
    ).agg(
        next_payment_date = pd.NamedAgg(column="payment_date", aggfunc=min)    
    )
    df_amortization_2["next_payment_date"] = pd.to_datetime(df_amortization_2["next_payment_date"]).dt.normalize()

    return df_amortization_1,df_amortization_2

def get_ops_data(
    df_snap:pd.DataFrame,
    df_loans:pd.DataFrame,
    df_amortization_1:pd.DataFrame,
    df_amortization_2:pd.DataFrame,
):
    df_base = pd.merge(
        df_snap[
            df_snap.groupby("application_id").month_end.transform(max) == df_snap.month_end
        ][
            [
                "application_id",
                "loan_amount",
                "payment_number",
                "dpd",
                "last_payment",
                "late_since",
                "payment_amount_paid",
                "principal_paid",
                "interest_paid",
                "iva_paid",
                "month_end",
                "bucket",
                "is_late"
            ]
        ],
        df_amortization_1,
        how="left",
        on="application_id"
    )

    df_base = pd.concat(
        [
            df_base,
            df_loans[
                ~df_loans.application_id.isin(df_base.application_id)
            ][
                [
                    "application_id",
                    "loan_amount"
                ]
            ].assign(
                payment_number = 1,
                is_late = False
            )
        ]
    )

    fillna_cols = [
        "payment_amount_paid",
        "principal_paid",
        "interest_paid",
        "iva_paid",
        "payment_amount_due",
        "principal_due",
        "interest_due",
        "iva_due"
    ]
    df_base[fillna_cols] = df_base[fillna_cols].fillna(0)


    df_base = pd.merge(
        df_base,
        df_amortization_2,
        how="left",
        on="application_id"
    )

    df_base["interest_and_iva_due"] = df_base["interest_due"] + df_base["iva_due"]
    df_base["next_payment_amount"] = df_base["payment_amount_due"] - df_base["payment_amount_paid"].fillna(0) + df_base["payment_amount"].fillna(0)
    df_base["payment_amount_remaining"] = df_base["payment_amount_due"] - df_base["payment_amount_paid"].fillna(0)
    df_base["principal_remaining"] = df_base["principal_due"] - df_base["principal_paid"].fillna(0)
    df_base["interest_remaining"] = df_base["interest_due"] - df_base["interest_paid"].fillna(0)
    df_base["iva_remaining"] = df_base["iva_due"] - df_base["iva_paid"].fillna(0)
    df_base["number_of_payments_paid"] = (df_base["payment_amount_paid"] // df_base["payment_amount"]).fillna(0)
    df_base["quotas_on_dpd"] = np.maximum(df_base["number_of_payments_due"] - df_base["number_of_payments_paid"],0)
    df_base["since_quota_on_dpd"] = (df_base["number_of_payments_paid"] + 1).fillna(0)

    return df_base

def run_pipeline(
    vintage:int,
    file_path:str|None=None,
    verbose:bool=False
):
    df_loans, df_snap,df_amortizacion = read_files()
    df_amortization_1,df_amortization_2 = transform_init_data(vintage,df_amortizacion)
    df_base = get_ops_data(df_snap,df_loans,df_amortization_1,df_amortization_2)
    


    if verbose:
        print(df_base.info())

    if file_path:
        df_base[
            [
                "application_id",
                "payment_number",
                "interest_and_iva_due",
                "payment_amount_due",
                "next_payment_date",
                "next_payment_amount",
                "is_late",
                "payment_amount_remaining",
                "principal_remaining",
                "iva_remaining",
                "interest_remaining",
                "number_of_payments_paid",
                "since_quota_on_dpd",
                "quotas_on_dpd",
                "late_since",
                "dpd"

            ]
        ].to_csv(
            f"{file_path}collect_operations.csv",
            index=False
        )

    print("- Collect Ops ha terminado")
