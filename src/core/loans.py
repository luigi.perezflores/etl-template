import pandas as pd
import numpy as np
from pandas.api.types import is_datetime64_any_dtype
from dateutil.parser import parse
import re

from google.spread import Spread
from src.utils.utils import format_column_name, format_to_int, rename_cols, filter_columns
from src.core.utils import get_data
from src.core.constants import (
    data_sources_google,
    data_sources_google_apps,
    str_columns_1, 
    str_columns_2,
    str_columns_3,
    str_columns_4, 
    timestamp_columns_dict, 
    date_columns_dict,
    rename_columns_dict,
    periodos_dict,
    final_vars_dict,
    integer_columns,
    values_as_nan,
    numeric_columns_dict,
    manual_apps_pricing,
    dict_tasas
)

def format_string_columns(
        df:pd.DataFrame
    ) -> pd.DataFrame:

    df_aux = df.copy()
    for var in str_columns_1:
        df_aux[var] = df_aux[var].apply(lambda x: re.sub(' +', ' ', str(x)).strip().lower())
        
    for var in str_columns_2:
        df_aux[var] = df_aux[var].apply(lambda x: re.sub(' +', ' ', str(x)).strip().capitalize())
        
    for var in str_columns_3:
        df_aux[var] = df_aux[var].apply(lambda x: re.sub(' +', ' ', str(x)).strip().title())
        
    for var in str_columns_4:
        df_aux[var] = df_aux[var].apply(lambda x: re.sub(' +', ' ', str(x)).strip().upper())


    df_aux = df_aux.replace(values_as_nan, np.NaN)

    return df_aux

def filter_data(
        df:pd.DataFrame
    ) -> pd.DataFrame:

    df_aux = df.copy()
    df_aux = df_aux[
        (df_aux.id_cliente.notna())
        & (df_aux.estado_final == 'Moto entregada')
    ]

    return df_aux

def transform_timestamp_cols(
        df:pd.DataFrame,
        timestamp_columns:list,
        source='online',
        source_version:int=2
    ) -> pd.DataFrame:

    df_aux = df.copy()
    for var in timestamp_columns:
        if is_datetime64_any_dtype(df[var]):
            pass
        else:
            if source=='online':
                dayfirst_value = True if source_version==1 else False 
                df_aux[var] = pd.to_datetime(
                df_aux[var].astype(str).str.split(" ").str[0],
                format="mixed",
                dayfirst=dayfirst_value,
                ).dt.strftime("%Y-%m-%d")
            elif source=='local':
                print(var)
                df_aux[var] = pd.to_datetime(
                    df_aux[var].astype(str).str.split(" ").str[0],
                    ).dt.strftime('%Y-%m-%d')
    return df_aux

def transform_date_cols(
        df:pd.DataFrame, 
        date_columns:list
    ) -> pd.DataFrame:

    df_aux = df.copy()
    for var in date_columns:
        df_aux[var] = pd.to_datetime(
            df_aux[var],
            dayfirst=True
        )

    return df_aux

def transform_numeric_cols(
        df:pd.DataFrame, 
        numeric_columns_dict:dict
    ) -> pd.DataFrame:

    df_aux = df.copy()
    for col, dtype in numeric_columns_dict.items():
        try:
            df_aux[col] = df_aux[col].astype(dtype)
        except:
            df_aux[col] = df_aux[col].astype(str).str.replace(",","").astype(dtype)

    return df_aux

def assign_frequency_id(
        df:pd.DataFrame, 
        frequency:str
    ) -> pd.DataFrame:

    df_aux = df.copy()
    df_aux["frequency_id"] = periodos_dict[frequency]

    return df_aux

def assign_id(
        df:pd.DataFrame, 
        sort_column:str='application_date'
    ) -> pd.DataFrame:

    df_aux = df.copy()
    df_aux = df_aux.sort_values(by=sort_column).reset_index(drop=True).reset_index().rename(columns={"index":"id"})

    return df_aux


def read_df_app(
    df:pd.DataFrame,
    dir_core:str="./data/core/outputs/"
):
    df_apps = pd.read_csv(
        f"{dir_core}applications.csv",
        sep=";"
    )

    df_apps = df_apps.sort_values(
        by=[
            "id",
            "score_generico"
        ]
    ).drop_duplicates(keep="first",subset="id")

    df_joinned = pd.merge(
        df,
        df_apps[
            [
                'id',
                'score_generico',
            ]
        ].rename(
            columns={
                "id":"application_id"
            }
        ),
        how='left',
        on="application_id"
    )

    return df_joinned



def assign_apr(
        df:pd.DataFrame, 
        source_version:int,
        dict_aprs: dict=dict_tasas
    ) -> pd.DataFrame:

    df['apr'] = np.where(
            df.application_id.isin(list(manual_apps_pricing.keys())),
            .575,
            np.where(
                df.application_date < "2023-05-26",
                df.tenor.apply(lambda x: dict_aprs[1][x]['ordinal']),
            np.where(
                df.application_date < "2023-10-05",
                df.tenor.apply(lambda x: dict_aprs[2][x]['ordinal']),
                df.tenor.apply(lambda x: dict_aprs[3][x]['ordinal']))
            )
        )

    #Buen Fin
    df['apr'] = np.where(
                (df.application_date >= '2023-11-17') & (df.application_date <= '2023-11-30'),
                np.where(
                    df.score_generico<=655,
                    df.tenor.apply(lambda x: dict_aprs[4][x]['LOW_BC_score']['ordinal']),
                    df.tenor.apply(lambda x: dict_aprs[4][x]['HIGH_BC_score']['ordinal'])
                ),
                df['apr']
        )

    #Empleados
    if source_version==2:
        df['apr'] = np.where(
                    (df.application_date >= "2024-01-12"),
                    np.where(
                        (df.es_empleado.isin(["Si","SI","si"])),
                        0.40,
                        df['apr']
                        ),
                    df['apr']
                    )
        if df['es_empleado'].isin(["Si","SI","si"]).sum()>0:
            print(df[df['es_empleado'].isin(["Si","SI","si"])])   
    return df

def run_loan_table(
        source_version:int, 
        file_path:str|None=None,
        source:str='online',
        vintage:list|None=None,
    ) -> pd.DataFrame:
  
    if source_version==1:
        base_final_db = data_sources_google_apps["primera_fuente"]["databases"]["base_final"]
    elif source_version==2:
        base_final_db = data_sources_google_apps["segunda_fuente"]["databases"]["base_final"]
    else:
        print('Source version incorrect')
    df = get_data(source_version, base_final_db,vintage,source)

    df = format_column_name(df)
    df = format_string_columns(df)
    df = filter_data(df)
    df = transform_timestamp_cols(df, timestamp_columns_dict[source_version],source,source_version)
    df = transform_date_cols(df, date_columns_dict[source_version])
    df = assign_frequency_id(df,'weekly')
    df = rename_cols(df, rename_columns_dict[source_version])
    df = transform_numeric_cols(df, numeric_columns_dict[source_version])
    df = assign_id(df)
    df = format_to_int(df, integer_columns)
    df = filter_columns(df, final_vars_dict[source_version])
    df = read_df_app(df)
    df = assign_apr(df,source_version)

    df = df.sort_values(
        by=[
            'name',
            'booking_date'
        ]
    ).drop_duplicates(
        subset='application_id',
        keep='first'
    ).reset_index(drop=True)

    if file_path:
        df.to_csv(
            file_path,
            index=False
        )

    return df

def run_pipeline(
        vintage:int,
        file_path:str|None=None,
#        versions:list=[1,2],
        source:str="online",
        verbose:bool=False
):
    versions = [1,2]
    lst_tables = []
    for version in versions:
        if verbose:
            print(version)

        df_aux = run_loan_table(
            version,
            source=source,
            vintage=vintage,
            file_path = f"./data/core/outputs/temp/loans_temp_{version}"
        )


        df_aux["source"] = version
        
        lst_tables.append(df_aux)
        
        if verbose:
            print(f"Done source: {version}")

    df = pd.concat(lst_tables)

    df = df.astype(
        {
            "application_id":int,
            "tenor":int
        }
    )

    if verbose:
        print(df.info())

    if file_path:
        df.to_csv(
            file_path,
            index=False
        )

    print("- Loans ha terminado")

    return df