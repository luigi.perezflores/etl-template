import numpy as np

cols_to_date = {
    "payments": [
        "created_date"
    ]
}

init_date = '2022-04-01'

data_sources_google = {
    1:{
        'id':"1mt0DcAb_ATFxkqEjpeMOy-gdnMpFt0OcPqqbSaOVg1E",
        'sheet_name':'Resumen data',
        'file':"./data/core/raw/motor/Waldos motos_{vintage}.xlsx",
    },
    2:{
        'id':"1nqgnsf02EKQHlbyO7mbmui1wyORIyEMhr2c-Ps6vC-Y",
        'sheet_name':'BASE FINAL',
        'file':"./data/core/raw/motor/WALDO'S - Datawarehouse_{vintage}.xlsx",
    }
}

estatus_validos = [
    'Rechazado',
    'Proceso incompleto'
    'Moto entregada',
    'Aprobado',
]

periodos_dict = {
    "daily":1,
    "weekly":2,
    "bi-weekly":3,
    "monthly":4,
}

str_columns_1 = [
    'correo',
]

str_columns_2 = [
    'validacion_celular',
    'validación_residencia',
    'validación_ocupación',
    'validación_lugar_de_trabajo',
    'filtro_score',
    'capacidad_de_pago',
    'estado_final',
    'causal_rechazo/aceptación',
]

str_columns_3 = [
    'nombre',
    'ref_moto_solicitada',
    'ref_moto_aprobada',
]

str_columns_4 = [
    'rfc',
]

timestamp_columns_dict = {
    1:[
        "timestamp_correo_aprobado",
        # "timestamp_contrato_firmado",
        # "timestamp_moto_entregada"
        "timestamp_moto_entregada_/_contrato_firmado",
    ],
    2:[
        "timestamp_form_1_solicitud",
        "timestamp_moto_entregada_/_contrato_firmado", #"timestamp_moto_entregada"
    ]
}

date_columns_dict = {
    1:[
        "fecha_aplicación",
    ],
    2:[
        "fecha_de_nacimiento",
    ]
}

integer_columns = [
    'application_id',
    'tenor'
]

rename_columns_dict = {
    1:{
        "id_cliente":"application_id",
        "fecha_aplicación":"application_date",
        # "timestamp_moto_entregada":"booking_date",
        "timestamp_moto_entregada_/_contrato_firmado":"booking_date",
        "valor_crédito":"loan_amount",
        "cuota_semanal":"payment_amount",
        "pct_enganche":"pct_downpayment",
        "valor_enganche":"downpayment",
        "plazos":"tenor",
        "nombre":"name",
    },
    2:{
        "id_cliente":"application_id",
        "timestamp_form_1_solicitud":"application_date",
        "timestamp_moto_entregada_/_contrato_firmado":"booking_date", # moto_entregada_/_contrato_firmado timestamp_moto_entregada
        "valor_crédito":"loan_amount",
        "cuota_semanal":"payment_amount",
        "pct_enganche":"pct_downpayment",
        "valor_enganche":"downpayment",
        "plazos":"tenor",
        "nombre":"name",
    }
}

final_vars_dict = {
    1:[
        "id",
        "application_id",
        "name", # Quitar
        "rfc", # Quitar
        "application_date",
        "booking_date",
        "loan_amount",
        "payment_amount",
        "pct_downpayment",
        "downpayment",
        "frequency_id",
        "tenor",
    ],
    2:[
        "id",
        "application_id",
        "name", # Quitar
        "rfc", # Quitar
        "application_date",
        "booking_date",
        "loan_amount",
        "payment_amount",
        "pct_downpayment",
        "downpayment",
        "frequency_id",
        "tenor",
        "es_empleado",
        "¿enganche_0?"        
    ]
}

values_as_nan = [
    '<NA>',
    'XAXX010101000',
    'S/N'
]

numeric_columns_dict = {
    1:{
        'loan_amount':float,
        'payment_amount':float,
        'downpayment':float
    },
    2:{
        'loan_amount':float,
        'payment_amount':float,
        'downpayment':float
    }
}

manual_apps_pricing = {
    1151244:{
        "apr":.575
    }
}

url_stores = 'https://waldos.com.mx/pages/sucursales'

str_columns_stores_2 = [
    'tienda',
    'sucursal',
    'estado',
    'municipio'
]

str_columns_stores_3 = [

    'dirección'
]

integer_columns = [
    'id'
]

rename_columns_store = {
    'tienda':'type_store',
    'número':'id',
    'sucursal':'store_name',
    'estado':'state',
    'municipio':'municipality',
    'dirección':'address',
}

final_vars = [
    'id',
    'type_store',
    'store_name',
    'state',
    'municipality',
    'address'
]

sheets = [
    'Resumen data',
    'Formulario',
    'Buro',
    'Capacidad pago',
    'Score NO HIT',
    'Score HIT',
    'Tablas validación',
    'Motos histórico'
]

deciles_original_buro = {
    '({value} <= 400)':np.nan,
    '({value} >= 400)&({value} <= 524)':1,
    '({value} >= 525)&({value} <= 538)':2,
    '({value} >= 539)&({value} <= 549)':3,
    '({value} >= 550)&({value} <= 559)':4,
    '({value} >= 560)&({value} <= 571)':5,
    '({value} >= 572)&({value} <= 584)':6,
    '({value} >= 585)&({value} <= 599)':7,
    '({value} >= 600)&({value} <= 614)':8,
    '({value} >= 615)&({value} <= 639)':9,
    '({value} >= 640)&({value} <= 654)':10,
    '({value} >= 655)&({value} <= 658)':11,
    '({value} >= 659)&({value} <= 667)':12,
    '({value} >= 668)&({value} <= 674)':13,
    '({value} >= 675)&({value} <= 682)':14,
    '({value} >= 683)&({value} <= 690)':15,
    '({value} >= 691)&({value} <= 698)':16,
    '({value} >= 699)&({value} <= 704)':17,
    '({value} >= 705)&({value} <= 712)':18,
    '({value} >= 713)&({value} <= 724)':19,
    '({value} >= 725)&({value} <= 760)':20
}

deciles_buro = {
    '({value} <= 400)':np.nan,
    '({value} >= 400)&({value} <= 639)':1,
    '({value} >= 640)&({value} <= 654)':2,
    '({value} >= 655)&({value} <= 667)':3,
    '({value} >= 668)&({value} <= 674)':4,
    '({value} >= 675)&({value} <= 682)':5,
    '({value} >= 683)&({value} <= 690)':6,
    '({value} >= 691)&({value} <= 698)':7,
    '({value} >= 699)&({value} <= 704)':8,
    '({value} >= 705)&({value} <= 712)':9,
    '({value} >= 713)&({value} <= 760)':10
}

deciles_hit = {
    '({value} <= 447)':1,
    '({value} >= 448)&({value} <= 546)':2 ,
    '({value} >= 547)&({value} <= 626)':3,
    '({value} >= 627)&({value} <= 700)':4,
    '({value} >= 701)&({value} <= 766)':5,
    '({value} >= 767)&({value} <= 831)':6 ,
    '({value} >= 832)&({value} <= 883)':7,
    '({value} >= 884)&({value} <= 923)':8,
    '({value} >= 924)&({value} <= 949)':9, # es diferente en pay?
    '({value} >= 950)':10
}

deciles_no_hit = {
    '({value} <= 528)':1,
    '({value} >= 529)&({value} <= 636)':2 ,
    '({value} >= 637)&({value} <= 718)':3,
    '({value} >= 719)&({value} <= 785)':4,
    '({value} >= 786)&({value} <= 841)':5,
    '({value} >= 842)&({value} <= 891)':6 ,
    '({value} >= 892)&({value} <= 930)':7,
    '({value} >= 931)&({value} <= 960)':8,
    '({value} >= 961)&({value} <= 981)':9,
    '({value} >= 982)':10
}

amount_columns = [
    'valor_moto_solicitada',
    'score_hit',
    'valor_total_moto_aprobada',
    'valor_enganche',
    'valor_crédito',
    'cuota_semanal',
    'decil_buro',
    'decil_hit'
]

meses_dict = {
    'enero': 1,
    'febrero': 2,
    'marzo': 3,
    'abril': 4,
    'mayo': 5,
    'junio': 6,
    'julio': 7,
    'agosto': 8,
    'septiembre': 9,
    'octubre': 10,
    'noviembre': 11,
    'diciembre': 12
}

dict_tasas = {
    1: {
        52:{
            'ordinal':.65,
            'mora':.7
        },
        104:{
            'ordinal':.575,
            'mora':.7
        }
    },
    2: {
        52:{
            'ordinal':.78,
            'mora':.95
        },
        104:{
            'ordinal':.725,
            'mora':.95
        }
    },
    3: {
        52:{
            'ordinal':.745,
            'mora':.90
        },
        104:{
            'ordinal':.70,
            'mora':.90
        }
    },
    4: {
        52:{
            "LOW_BC_score":{
                'ordinal': .69,
                'mora': .90
            },
            "HIGH_BC_score":{
                'ordinal': .68,
                'mora': .90
            }
        },
        104:{
            "LOW_BC_score":{
                'ordinal': .675,
                'mora':.90
            },
            "HIGH_BC_score":{
                'ordinal': .55,
                'mora':.90
            }
        }
    }
}


#applications
data_sources_google_apps = {
    "primera_fuente": {
        "google_sheet_id": "1mt0DcAb_ATFxkqEjpeMOy-gdnMpFt0OcPqqbSaOVg1E",
        "databases": {
            "base_final": {
                "sheet_name": "Resumen data",
                "skip_rows": 0,
                "drop_table_if_exists": True,
                "columns": [
                    "id_cliente",
                    "rfc",
                    "tienda",
                    "estado_final", 
                    "causal_rechazo/aceptación", 
                    "moto_entregada_/_contrato_firmado",
                    "ref_moto_solicitada", 
                    "ref_moto_aprobada",
                    "plazos",
                    "pct_enganche",
                    "valor_enganche",
                    "score_genérico_buró",
                    "score_hit",
                    "score_no_hit",
                    "fecha_aplicación",
                    "timestamp_correo_aprobado",
                    "timestamp_correo_rechazado",
                    "timestamp_moto_entregada_/_contrato_firmado",
                    "timestamp_factura",
                    ],
                "rename_columns": {
                    "id_cliente": "id",
                    "ref_moto_solicitada": "requested_product",
                    "ref_moto_aprobada": "approved_product",
                    "plazos": "tenor",
                    "pct_enganche": "pct_downpayment",
                    "valor_enganche": "downpayment",
                    "score_genérico_buró": "score_generico",
                    "fecha_aplicación": "created_date",
                    "timestamp_correo_aprobado": "approved_email_date",
                    "timestamp_correo_rechazado": "rejected_email_date",
                    "timestamp_moto_entregada_/_contrato_firmado": "signed_contract_date",
                    "timestamp_factura": "invoice_date",
                },
                "date_columns": [
                    "created_date",
                    "approved_email_date",
                    "rejected_email_date",
                    "signed_contract_date",
                    "invoice_date",
                ],
                "str_columns": [
                    "rfc",
                    "requested_product",
                    "approved_product",
                    "rejection_cause_risk",
                    "rejection_cause_kyc",
                    "rejection_cause",
                ],
                "numeric_columns": [
                    "id",
                    "score_generico",
                    "score_hit",
                    "score_no_hit",
                    "tenor",
                    "pct_downpayment",
                    "downpayment",
                    "store_id",
                    "status_id",
                    "booked",
                    "completed",
                    "risk_decision",
                    "frequency",
                    "approved",
                    "signed_contract",
                ],
                "final_columns": [
                    "id",
                    "rfc",
                    "score_generico",
                    "score_hit",
                    "score_no_hit",
                    "requested_product",
                    "approved_product",
                    "tenor",
                    "pct_downpayment",
                    "downpayment",
                    "created_date",
                    "store_id",
                    "status_id",
                    "booked",
                    "completed",
                    "risk_decision",
                    "rejection_cause_risk",
                    "rejection_cause_kyc",
                    "rejection_cause",
                    "frequency",
                    "approved",
                    "signed_contract",
                    "signed_contract_date",
                    "approved_email_date",
                    "rejected_email_date",
                    "invoice_date",
                ],
                "data_types": {
                    "id": "Int64",
                    "rfc": "str",
                    "score_generico": "float64",
                    "score_hit": "float64",
                    "score_no_hit": "float64",
                    "requested_product": "str",
                    "approved_product": "str",
                    "tenor": "Int64",
                    "pct_downpayment": "float64",
                    "downpayment": "float64",
                    "created_date": "datetime64[ns]",
                    "store_id": "Int64",
                    "status_id": "Int64",
                    "booked": "Int64",
                    "completed": "Int64",
                    "risk_decision": "Int64",
                    "rejection_cause_risk": "str",
                    "rejection_cause_kyc": "str",
                    "rejection_cause": "str",
                    "frequency": "Int64",
                    "approved": "Int64",
                    "signed_contract": "Int64",
                    "signed_contract_date": "datetime64[ns]",
                    "approved_email_date": "datetime64[ns]",
                    "rejected_email_date": "datetime64[ns]",
                    "invoice_date": "datetime64[ns]",
                },
            },
            "capacidad_pago": {
                "sheet_name": "Capacidad pago",
                "skip_rows": 0,
                "drop_table_if_exists": True,
                "columns": [
                    "id_cliente",
                    "remanente"
                    ],
                "rename_columns": {
                    "id_cliente": "id",
                    "remanente": "payment_capacity"
                },
                "numeric_columns": [
                    "id",
                    "payment_capacity"
                ],
                "data_types": {
                    "id": "Int64",
                    "payment_capacity": "float64"
                }
            },
        }
    },
    "segunda_fuente": {
        "google_sheet_id": "1nqgnsf02EKQHlbyO7mbmui1wyORIyEMhr2c-Ps6vC-Y",
        "databases": {
            "base_final": {
                "sheet_name": "BASE FINAL",
                "skip_rows": 0,
                "drop_table_if_exists": True,
                "columns": [
                    "id_cliente",
                    "rfc",
                    "tienda",
                    "estado_final", 
                    "causal_rechazo/aceptación", 
                    "moto_entregada_/_contrato_firmado",
                    "ref_moto_solicitada", 
                    "ref_moto_aprobada",
                    "plazos",
                    "pct_enganche",
                    "valor_enganche",
                    "score_genérico_buró",
                    "score_hit",
                    "score_no_hit",
                    "estado_del_contrato",
                    "fecha_aplicación",
                    "timestamp_form_1_solicitud",
                    "timestamp_form_2_documentos",
                    "timestamp_form_3",
                    "timestamp_form_4",
                    "timestamp_form_5",
                    "timestamp_correo_aprobado",
                    "timestamp_correo_rechazado",
                    "timestamp_moto_entregada_/_contrato_firmado",
                    "timestamp_factura",
                    "timestamp_fecha_creación_calculadora",
                    "timestamp_pld",
                    "rechazo_pld",
                    "¿duplicado?"
                    ],
                "rename_columns": {
                    "id_cliente": "id",
                    "ref_moto_solicitada": "requested_product",
                    "ref_moto_aprobada": "approved_product",
                    "plazos": "tenor",
                    "pct_enganche": "pct_downpayment",
                    "valor_enganche": "downpayment",
                    "score_genérico_buró": "score_generico",
                    "fecha_aplicación": "created_date",
                    "timestamp_form_1_solicitud": "request_date",
                    "timestamp_form_2_documentos": "document_date",
                    "timestamp_form_3": "timestamp_form_3",
                    "timestamp_form_4": "timestamp_form_4",
                    "timestamp_form_5": "timestamp_form_5",
                    "timestamp_correo_aprobado": "approved_email_date",
                    "timestamp_correo_rechazado": "rejected_email_date",
                    "timestamp_moto_entregada_/_contrato_firmado": "signed_contract_date",
                    "timestamp_factura": "invoice_date",
                    "timestamp_fecha_creación_calculadora": "calculator_date",
                    "estado_del_contrato": "contract_status",
                    "¿duplicado?": "is_duplicated"
                },
                "date_columns": [
                    "created_date",
                    "request_date",
                    "document_date",
                    "timestamp_form_3",
                    "timestamp_form_4",
                    "timestamp_form_5",
                    "approved_email_date",
                    "rejected_email_date",
                    "signed_contract_date",
                    "invoice_date",
                    "calculator_date",
                    "timestamp_pld"
                ],
                "str_columns": [
                    "rfc",
                    "requested_product",
                    "approved_product",
                    "rejection_cause_risk",
                    "rejection_cause_kyc",
                    "rejection_cause",
                    "contract_status",
                    "rechazo_pld",
                    "is_duplicated"
                ],
                "numeric_columns": [
                    "id",
                    "score_generico",
                    "score_hit",
                    "score_no_hit",
                    "tenor",
                    "pct_downpayment",
                    "downpayment",
                    "store_id",
                    "status_id",
                    "booked",
                    "completed",
                    "risk_decision",
                    "frequency",
                    "approved",
                    "signed_contract",
                ],
                "final_columns": [
                    "id",
                    "rfc",
                    "score_generico",
                    "score_hit",
                    "score_no_hit",
                    "requested_product",
                    "approved_product",
                    "tenor",
                    "pct_downpayment",
                    "downpayment",
                    "created_date",
                    "store_id",
                    "status_id",
                    "booked",
                    "completed",
                    "risk_decision",
                    "rejection_cause_risk",
                    "rejection_cause_kyc",
                    "rejection_cause",
                    "frequency",
                    "approved",
                    "signed_contract",
                    "signed_contract_date",
                    "approved_email_date",
                    "rejected_email_date",
                    "invoice_date",
                    "request_date",
                    "document_date",
                    "timestamp_form_3",
                    "timestamp_form_4",
                    "timestamp_form_5",
                    "calculator_date",
                    "fill_basic_documentation",
                    "decision_time",
                    "credit_formalization_contract_signing",
                    "contract_status",
                    "timestamp_pld",
                    "rechazo_pld",
                    "is_duplicated"
                ],
                "data_types": {
                    "id": "Int64",
                    "rfc": "str",
                    "score_generico": "float64",
                    "score_hit": "float64",
                    "score_no_hit": "float64",
                    "requested_product": "str",
                    "approved_product": "str",
                    "tenor": "Int64",
                    "pct_downpayment": "float64",
                    "downpayment": "float64",
                    "created_date": "datetime64[ns]",
                    "store_id": "Int64",
                    "status_id": "Int64",
                    "booked": "Int64",
                    "completed": "Int64",
                    "risk_decision": "Int64",
                    "rejection_cause_risk": "str",
                    "rejection_cause_kyc": "str",
                    "rejection_cause": "str",
                    "frequency": "Int64",
                    "approved": "Int64",
                    "signed_contract": "Int64",
                    "signed_contract_date": "datetime64[ns]",
                    "approved_email_date": "datetime64[ns]",
                    "rejected_email_date": "datetime64[ns]",
                    "invoice_date": "datetime64[ns]",
                    "request_date": "datetime64[ns]",
                    "document_date": "datetime64[ns]",
                    "timestamp_form_3": "datetime64[ns]",
                    "timestamp_form_4": "datetime64[ns]",
                    "timestamp_form_5": "datetime64[ns]",
                    "calculator_date": "datetime64[ns]",
                    "fill_basic_documentation": "float64",
                    "decision_time": "float64",
                    "credit_formalization_contract_signing": "float64",
                    "contract_status": "str",
                    "timestamp_pld": "datetime64[ns]",
                    "rechazo_pld": "str",
                    "is_duplicated": "str"
                }
            },
            "capacidad_pago": {
                "sheet_name": "Capacidad de Pago",
                "skip_rows": 0,
                "drop_table_if_exists": True,
                "columns": [
                    "id",
                    "remanente"
                    ],
                "rename_columns": {
                    "remanente": "payment_capacity"
                },
                "numeric_columns": [
                    "id",
                    "payment_capacity"
                ],
                "data_types": {
                    "id": "Int64",
                    "payment_capacity": "float64"
                }
            },
            "mesa_control_comprobante_nomina": {
                "sheet_name": "Mesa de Control (Comprobantes nomina)",
                "skip_rows": 0,
                "drop_table_if_exists": True,
                "columns": [
                    "rfc_del_cliente",
                    "confirmacion_validación_de_documentos",
                    "timestamp"
                    ],
                "rename_columns": {
                    "rfc_del_cliente": "rfc",
                    "confirmacion_validación_de_documentos": "confirmation_validation_of_documents",
                    "timestamp": "control_table_payroll_check_date"
                },
                "date_columns": [
                    "control_table_payroll_check_date",
                ],
                "str_columns": [
                    "confirmation_validation_of_documents"
                ],
                "data_types": {
                    "rfc": "str",
                    "confirmation_validation_of_documents": "str",
                    "control_table_payroll_check_date": "datetime64[ns]",
                }
            },
            "seguimiento_contratos": {
                "sheet_name": "Seguimiento contratos",
                "skip_rows": 0,
                "drop_table_if_exists": True,
                "columns": [
                    "rfc",
                    "resultado_de_documentación",
                    "estado_actual"
                    ],
                "rename_columns": {
                    "resultado_de_documentación": "documentation_result",
                    "estado_actual": "actual_state"
                },
                "str_columns": [
                    "documentation_result",
                    "actual_state"
                ],
                "data_types": {
                    "rfc": "str",
                    "documentation_result": "str",
                    "actual_state": "str",
                }
            },
                "estados_clientes": {
                "sheet_name": "Resumen estados clientes",
                "skip_rows": 0,
                "drop_table_if_exists": True,
                "columns": [
                    "rfc",
                    "validación_de_documentos",
                    "validación_de_referencias",
                    "resultado_estudio_de_crédito",
                    "resultado_pld"
                    ],
                "rename_columns": {
                    "validación_de_documentos": "validacion_de_documentos",
                    "validación_de_referencias": "validacion_de_referencias",
                    "resultado_estudio_de_crédito": "estudio_de_credito",
                },
                "final_columns": [
                    "rfc",
                    "validacion_de_documentos",
                    "validacion_de_referencias",
                    "estudio_de_credito",
                    "resultado_pld"
                ],
                "data_types": {
                    "rfc": "str",
                    "validacion_de_documentos": "str",
                    "validacion_de_referencias": "str",
                    "estudio_de_credito": "str",
                    "resultado_pld": "str"
                } 
            },
            "form_responses_2": {
                "sheet_name": "Form Responses 2",
                "skip_rows": 0,
                "drop_table_if_exists": True,
                "columns": [
                    "rfc_-_curp",
                    "timestamp",
                    "¿que_tipo_de_acción_estás_realizando?"
                    ],
                "rename_columns": {
                    "rfc_-_curp": "rfc",
                    "timestamp": "form_responses_2_date",
                    "¿que_tipo_de_acción_estás_realizando?": "form_responses_2_action_type"
                },
                "str_columns": [
                    "form_responses_2_action_type"
                ],
                "date_columns": [
                    "form_responses_2_date",
                ],
                "data_types": {
                    "rfc": "str",
                    "form_responses_2_date": "datetime64[ns]",
                    "form_responses_2_action_type": "str",
                }
            },
            "validaciones": {
                "sheet_name": "Referencias Validaciones Extra",
                "skip_rows": 0,
                "drop_table_if_exists": True,
                "columns": [
                    "rfc_del_cliente",
                    "timestamp"
                    ],
                "rename_columns": {
                    "rfc_del_cliente": "rfc",
                    "timestamp": "validation_date"
                },
                "date_columns": [
                    "validation_date"
                ],
                "final_columns": [
                    "rfc",
                    "validation_date"
                ],
                "data_types": {
                    "rfc": "str",
                    "validation_date": "datetime64[ns]",
                }
            },
            "buro": {
                "sheet_name": "Mesa de Control (Buro)",
                "skip_rows": 1,
                "drop_table_if_exists": True,
                "columns": [
                    "rfc_del_cliente",
                    "timestamp"
                    ],
                "rename_columns": {
                    "rfc_del_cliente": "rfc",
                    "timestamp": "buro_date"
                },
                "date_columns": [
                    "buro_date"
                ],
                "final_columns": [
                    "rfc",
                    "buro_date"
                ],
                "data_types": {
                    "rfc": "str",
                    "buro_date": "datetime64[ns]",
                }
            },
        }
    },
}

data_sources_google_daily_report = {
       "segunda_fuente": {
        "google_sheet_id": "1nqgnsf02EKQHlbyO7mbmui1wyORIyEMhr2c-Ps6vC-Y",
        "databases": {
            "base_final": {
                "sheet_name": "BASE FINAL",
                "skip_rows": 0,
                "drop_table_if_exists": True,
                "columns": [
                    "id_cliente",
                    "nombre",
                    "rfc",
                    "estado_del_contrato",
                    "tienda",
                    "estado_final", 
                    "causal_rechazo/aceptación", 
                    "fecha_aplicación",
                    "timestamp_form_1_solicitud",
                    "timestamp_form_2_documentos",
                    "timestamp_form_3",
                    "timestamp_form_4",
                    "timestamp_form_5",
                    "timestamp_correo_aprobado",
                    "timestamp_correo_rechazado",
                    "timestamp_moto_entregada_/_contrato_firmado",
                    "timestamp_factura",
                    ],
                "rename_columns": {
                    "id_cliente": "id",
                },
                "date_columns": [
                    "fecha_aplicación",
                    "timestamp_form_1_solicitud",
                    "timestamp_form_2_documentos",
                    "timestamp_form_3",
                    "timestamp_form_4",
                    "timestamp_form_5",
                    "timestamp_correo_aprobado",
                    "timestamp_correo_rechazado",
                    "timestamp_moto_entregada_/_contrato_firmado",
                    "timestamp_factura",
                ],
                "str_columns": [
                    "nombre",
                    "rfc",
                    "estado_final",
                    "estado_del_contrato"
                ],
                "numeric_columns": [
                    "id",
                ],
                "final_columns": [
                    "id",
                    "nombre",
                    "rfc",
                    "estado_del_contrato",
                    "estado_final",
                    "tienda",
                    "fecha_aplicación",
                    "timestamp_form_1_solicitud",
                    "timestamp_form_2_documentos",
                    "timestamp_form_3",
                    "timestamp_form_4",
                    "timestamp_form_5",
                    "timestamp_correo_aprobado",
                    "timestamp_correo_rechazado",
                    "timestamp_moto_entregada_/_contrato_firmado",
                    "timestamp_factura",
                ],
                "data_types": {
                   "id": "Int64",
                    "nombre": "str",
                    "rfc": "str",
                    "estado_final": "str",
                    "tienda": "str",
                    "fecha_aplicación": "datetime64[ns]",
                    "timestamp_form_1_solicitud": "datetime64[ns]",
                    "timestamp_form_2_documentos": "datetime64[ns]",
                    "timestamp_form_3": "datetime64[ns]",
                    "timestamp_form_4": "datetime64[ns]",
                    "timestamp_form_5": "datetime64[ns]",
                    "timestamp_correo_aprobado": "datetime64[ns]",
                    "timestamp_correo_rechazado": "datetime64[ns]",
                    "timestamp_moto_entregada_/_contrato_firmado": "datetime64[ns]",
                    "timestamp_factura": "datetime64[ns]",
                }
            },
            "validaciones": {
                "sheet_name": "Referencias Validaciones Extra",
                "skip_rows": 0,
                "drop_table_if_exists": True,
                "columns": [
                    "rfc_del_cliente",
                    "resultado_final_de_verificacion",
                    "timestamp"
                    ],
                "rename_columns": {
                    "rfc_del_cliente": "rfc",
                    "timestamp": "validation_date"
                },
                "date_columns": [
                    "validation_date"
                ],
                "final_columns": [
                    "rfc",
                    "resultado_final_de_verificacion",
                    "validation_date"
                ],
                "data_types": {
                    "rfc": "str",
                    "resultado_final_de_verificacion": "str",
                    "validation_date": "datetime64[ns]",
                }
            },
            "buro": {
                "sheet_name": "Mesa de Control (Buro)",
                "skip_rows": 1,
                "drop_table_if_exists": True,
                "columns": [
                    "rfc_del_cliente",
                    "estatus_consulta_y_validación",
                    "timestamp"
                    ],
                "rename_columns": {
                    "rfc_del_cliente": "rfc",
                    "timestamp": "buro_date"
                },
                "date_columns": [
                    "buro_date"
                ],
                "final_columns": [
                    "rfc",
                    "estatus_consulta_y_validación",
                    "buro_date"
                ],
                "data_types": {
                    "rfc": "str",
                    "estatus_consulta_y_validación": "str",
                    "buro_date": "datetime64[ns]",
                }
            },
            "mesa_control_comprobante_nomina": {
                "sheet_name": "Mesa de Control (Comprobantes nomina)",
                "skip_rows": 0,
                "drop_table_if_exists": True,
                "columns": [
                    "rfc_del_cliente",
                    "confirmacion_validación_de_documentos",
                    "timestamp"
                    ],
                "rename_columns": {
                    "rfc_del_cliente": "rfc",
                    "confirmacion_validación_de_documentos": "causa_estado_proceso_incompleto",
                    "timestamp": "fecha_causa_estado_proceso_incompleto"
                },
                "date_columns": [
                    "fecha_causa_estado_proceso_incompleto"
                ],
                "final_columns": [
                    "rfc",
                    "causa_estado_proceso_incompleto",
                    "fecha_causa_estado_proceso_incompleto"
                ],
                "data_types": {
                    "rfc": "str",
                    "causa_estado_proceso_incompleto": "str",
                    "fecha_causa_estado_proceso_incompleto": "datetime64[ns]"
                }
            },
            "estados_clientes": {
                "sheet_name": "Resumen estados clientes",
                "skip_rows": 0,
                "drop_table_if_exists": True,
                "columns": [
                    "rfc",
                    "validación_de_documentos",
                    "validación_de_referencias",
                    "resultado_estudio_de_crédito",
                    "resultado_pld"
                    ],
                "rename_columns": {
                    "validación_de_documentos": "validacion_de_documentos",
                    "validación_de_referencias": "validacion_de_referencias",
                    "resultado_estudio_de_crédito": "estudio_de_credito",
                },
                "final_columns": [
                    "rfc",
                    "validacion_de_documentos",
                    "validacion_de_referencias",
                    "estudio_de_credito",
                    "resultado_pld"
                ],
                "data_types": {
                    "rfc": "str",
                    "validacion_de_documentos": "str",
                    "validacion_de_referencias": "str",
                    "estudio_de_credito": "str",
                    "resultado_pld": "str"
                } 
            },
            "form_responses_2": {
                "sheet_name": "Form Responses 2",
                "skip_rows": 0,
                "drop_table_if_exists": True,
                "columns": [
                    "rfc_-_curp",
                    "¿que_tipo_de_acción_estás_realizando?",
                    "timestamp"
                    ],
                "rename_columns": {
                    "rfc_-_curp": "rfc",
                    "¿que_tipo_de_acción_estás_realizando?": "accion_realizada",
                    "timestamp": "fecha_accion_realizada",
                },
                "date_columns": [
                    "fecha_accion_realizada"
                ],
                "final_columns": [
                    "rfc",
                    "accion_realizada",
                    "fecha_accion_realizada"
                ],
                "data_types": {
                    "rfc": "str",
                    "accion_realizada": "str",
                    "fecha_accion_realizada": "datetime64[ns]"
                } 
            }
        }
    },
}
