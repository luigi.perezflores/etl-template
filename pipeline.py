import sys
import pandas as pd
import time

from src.core.applications import run_pipeline as run_applications
from src.core.daily_waldos_report import run_pipeline as run_daily_waldos_report
from src.core.loans import run_pipeline as run_loans
from src.core.payments import run_pipeline as run_payments
from src.core.snapshot_m import run_pipeline as run_snapshot
from src.core.collections_ops import run_pipeline as run_collections_ops
from src.core.expected_recovery import run_pipeline as run_expected_recovery
from src.core.utils import process_payments_dir
from config_pipeline import (
    dir_path,
    loans_name
)

def run_pipeline(
    vintage:int,
    source:str
):
    start = time.time()
    print("ETL de Waldos Motos ha iniciado")

    process_payments_dir(
        vintage
        )

    run_applications(
        vintage,
        dir_path,
        source
    )

    run_daily_waldos_report(
        vintage,
        dir_path,
         'online'   # Aún no funciona local por temas de formato de fechas
    )

    run_loans(
        vintage,
        f"{dir_path}{loans_name}",
        source
    )

    run_payments(
        vintage,
        dir_path
    )

    run_snapshot(
       vintage,
       dir_path
    )

    run_collections_ops(
       vintage,
       dir_path
    )

    run_expected_recovery(
        dir_path
    )

    end = time.time()
    duration_time = end - start

    print("ETL de Waldos Motos ha terminado")
    print(f"Tiempo total: {duration_time:.1f} segundos")

if __name__ == "__main__":
    try:
        vintage = int(sys.argv[1])
        source = str(sys.argv[2])
    except IndexError:
        vintage = int(pd.to_datetime("today").strftime("%Y%m%d"))
        source = 'online'
    run_pipeline(
        vintage,
        source
    )