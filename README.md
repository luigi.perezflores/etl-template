# etl-template

El proyecto ETL tiene como objetivo la construcción del modelo de datos del esquema. 
Los datos se extraen de diversas fuentes, como hojas de cálculo de Google
Sheets y carpetas de Google Drive, y se procesan para crear tablas que componen el modelo de datos.
Las tablas resultantes se almacenan en diferentes ubicaciones de Google Drive para su posterior
análisis y uso.

## Tabla de contenido

  - [Tabla de contenido](#tabla-de-contenido)
  - [Fuentes de datos](#fuentes-de-datos)
    - [Actuales](#actuales)
    - [Deprecadas](#deprecadas)
  - [Persistencia](#persistencia)
  - [Requerimientos](#requerimientos)
  - [¿Cómo correr?](#cómo-correr)
    - [Core](#core)
    - [Provisiones](#provisiones)

## Fuentes de datos

### Actuales

  
### Deprecadas


## Persistencia

- data
  - applications.csv
  - payments.csv
  - payments_with_local_calculator_v3.csv
  - amortization.csv
  - loans.csv
  - snapshot.csv
  - monthly_bucket_recoveries.csv
  - transition_matrix_total_amount.csv
- inputs
  - collect_operations.csv
- payments
  - payments_with_local_calculator_v3.csv

## Requerimientos

- Python: 3.10.12
- Instalación de requirements.txt

## ¿Cómo correr?

### Core (Daily)

#### Para ejecutar con los datos más actualizados del día que corre:
1. Descargar la carpeta de Payments [Payments] Almacenarlo en el directorio `./data/core/raw/payments`.
2. Abrir terminal y correr el siguiente comando:

          python pipeline.py

   Opcional: vintage con la fecha del día YYYYYMMDD y online como valor para source.            

3. Realizar la persistencia de los archivos en drive conforme a la sección [Persistencia](#persistencia)


#### Para ejecutar con datos de un día previo:

1. Asegurarse de tener las carpetas de payments y los motores del vintage correspondiente descargados en los subdirectorios correspondientes de `./data/core/raw/`.
2. Abrir terminal y correr el siguiente comando, donde YYYYYMMDD la fecha que se está procesando 

           python pipeline.py YYYYYMMDD local

3. Realizar la persistencia de los archivos en drive conforme a la sección [Persistencia](#persistencia)

#### Para ejecutar local del día que corre:

1. Descargar las [fuentes de datos actuales](#actuales) y agrégueles a los motores el vintage (_YYYYMMDD) al final de los nombres
de los archivos. Almacénelos en los subdirectorios correspondientes en `./data/core/raw/`. En el caso de payments la extracción del zip y renombrar la carpeta se hace de manera automática. Asegúrese de que existen estos dos archivos antes de comenzar a correr la ETL:
   - ./data/core/raw/motor/producto_YYYYYMMDD.xlsx
   - ./data/core/raw/motor/producto- Datawarehouse_YYYYYMMDD.xlsx

Después de ver en terminal el mensaje "Archivo de payments extraído" debe aparecer en el directorio de payments una carpeta con la fecha del día:   
   - ./data/core/raw/payments/YYYYYMMDD/

2. Abrir terminal y correr el siguiente comando, donde YYYYYMMDD es la fecha que se está procesando 

           python pipeline.py YYYYYMMDD 

     Ejemplo: Si se ejecutara el 30 de agosto de 2023, el comando correcto sería
  
         python pipeline.py 20230830

3. Realizar la persistencia de los archivos en drive conforme a la sección [Persistencia](#persistencia)

### Provisiones (Monthly)

1. Validar que se tengan actualizados los siguientes archivos:
   - loans.csv
   - snapshot_m.csv
   - applications.csv

2. Abrir terminal y correr el siguiente comando, donde YYYYMMDD es la fecha del día posterior al cierre que se quiere correr, 

         python pipeline_finance.py YYYYMMDD


   Ejemplo: si se quieren correr la provisiones hasta el cierre de agosto 2023, el comando correcto sería

         python pipeline_finance.py 20230901

